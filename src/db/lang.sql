DELETE FROM Sys_Language WHERE sID like 'lyapp%';

-- 登录页
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.appName', N'绿印新标APP', N'绿印新标APP', N'綠印新標APP', N'APP');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.autoLogin', N'自动登录', N'自动登录', N'自動登錄', N'Auto Logon');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.logon', N'登录', N'登录', N'登錄', N'logon');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.config', N'服务端配置', N'服务端配置', N'服務端配置', N'Server Configuration');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.logout', N'注销', N'注销', N'註銷', N'logout');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)

-- 登录校验
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.inputAccount', N'请输入账号', N'请输入账号', N'請輸入賬號', N'Please enter an account');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.inputRightAccount', N'请输入正确账号', N'请输入正确账号', N'請輸入正確賬號', N'Please enter the correct account');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.inputPassword', N'请输入密码', N'请输入密码', N'請輸入密碼', N'Please enter the password');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.login.inputRightPassword', N'请输入正确密码', N'请输入正确密码', N'請輸入正確密碼', N'Please enter the correct password');

-- 语言
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.lang.languageSelect', N'语言选择', N'语言选择', N'語言選擇', N'Language');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.lang.zhCN', N'中文简体', N'中文简体', N'中文简体', N'中文简体');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.lang.zhTW', N'中文繁体', N'中文繁体', N'中文繁體', N'中文繁体');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.lang.en', N'English', N'English', N'English', N'English');

-- 菜单
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.menu.menu', N'菜单', N'菜单', N'菜單', N'Menu');


-- 按钮
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.button.filter', N'筛选', N'筛选', N'篩選', N'Filter');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.button.query', N'查询', N'查询', N'查詢', N'Query');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.button.ok', N'确定', N'确定', N'確定', N'Ok');


-- 接口交互
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.intf.loading', N'加载中...', N'加载中...', N'加载中...', N'Loading...');

-- 单位
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.unit.millionYuan', N'万元', N'万元', N'萬元', N'Million Yuan');

-- 公共字段信息
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.share.queryDate', N'时间段', N'时间段', N'時間段', N'Query Date');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.share.customerName', N'客户名称', N'客户名称', N'客戶名稱', N'Customer Name');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.share.productCategory', N'产品分类', N'产品分类', N'產品分類', N'Product Category');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.share.department', N'部门', N'部门', N'部門', N'Department');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.share.salesPerson', N'销售人员', N'销售人员', N'銷售人員', N'Sales Person');
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.share.documentaryPerson', N'跟单人员', N'跟单人员', N'跟單人員', N'Documentary Person');
-- 销售管理字段信息

-- 销售额走势
INSERT INTO Sys_Language (uGuid1, sID, sLanguageDefault, sLanguageCHS, sLanguageCHT, sLanguageENG)
VALUES(N'DE83E953-7D1C-11EE-A331-BBC15146DBF5', N'lyapp.saletop.salesTrend', N'销售额走势', N'销售额走势', N'銷售額走勢', N'Sales Trend');

-- 生产管理字段信息

