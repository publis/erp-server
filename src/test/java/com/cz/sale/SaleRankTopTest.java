package com.cz.sale;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sale.dao.SaleRankTopMapper;
import com.cz.core.sale.dto.OrderDeliveryWarnResponse;
import com.cz.core.sale.dto.SaleRankTopRequest;
import com.cz.core.sale.dto.SaleRankTopRespnse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.cz.core.sale.dao.SalSalesOrderMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class SaleRankTopTest {

    @Autowired
    private SaleRankTopMapper saleRankTopMapper;

    @Test
    public void saleRankTest() {
        SaleRankTopRequest saleRankTopRequest = new SaleRankTopRequest();
        saleRankTopRequest.setStatistics("1");
        Map params = new HashMap();
        BeanUtil.beanToMap(saleRankTopRequest, params, false, false);
        Page<SaleRankTopRespnse> all = saleRankTopMapper.findSaleRankTopList(new Page<SaleRankTopRespnse>(1,10) , params);

        all.getRecords().forEach(element -> element.getDOMoneyTotal());
        List<SaleRankTopRespnse> records = all.getRecords();
        Integer i = 0;
        for (SaleRankTopRespnse record : records) {
            record.setDOMoneyTotal(record.getSGroupDimension() + "第"+ i++ +"名");
        }

        all.setRecords(records);

    }
}
