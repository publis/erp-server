package com.cz.sale;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sale.dao.SalSalesOrderMapper;
import com.cz.core.sale.dto.OrderDeliveryWarnRequest;
import com.cz.core.sale.dto.OrderDeliveryWarnResponse;
import com.cz.uitl.JdbcSqlTool;
import com.cz.workorder.dao.ManProductionRegistrationRepository;
import com.cz.workorder.entity.ManProductionRegistration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class SaleServiceTests {

	@Autowired
	private JdbcSqlTool jdbcSqlTool;

	@Autowired
	private SalSalesOrderMapper salSalesOrderMapper;

	@Resource
	private ManProductionRegistrationRepository repository;


	@Test
	public void test1() {
		final List<Map<String, Object>> maps = jdbcSqlTool.queryMapList("select top 10 * from Sal_SalesOrder_Detail");
		maps.stream().forEach(it->{
			System.out.println(it);
		});
	}

	@Test
	public void test2() {
//		OrderDeliveryWarnRequest params = new OrderDeliveryWarnRequest();
//		final Page<OrderDeliveryWarnResponse> page = salSalesOrderMapper.findPage(new Page(1, 10), params);
		//final List<OrderDeliveryWarnResponse> all = salSalesOrderMapper.findAll(new Page<OrderDeliveryWarnResponse>(1,10) , new HashMap<>());
		//System.out.println(all);

		ManProductionRegistration productionRegistration = new ManProductionRegistration();
		productionRegistration.setUGuid1(StrUtil.uuid());
		repository.insert(productionRegistration);
		System.out.println(productionRegistration);
	}
}
