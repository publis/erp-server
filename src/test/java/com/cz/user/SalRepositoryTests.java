package com.cz.user;

import com.cz.core.sale.dao.SaleRankTopMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class SalRepositoryTests {

    @Autowired
    private SaleRankTopMapper salRepo;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void getCustomerName(){
        /*for (SalSalesOrderEntity n210048 : salRepo.findPagingData("N210048")) {
            System.out.println(n210048.getCustomerName());
        }*/

      //第一种Query查法：需定义好接收返回对象，但可支持条件生成好sql
        String sql = "SELECT t1.sTaxName \n" +
                "FROM Sal_SalesOrder_Detail t1\n" +
                "inner join Sal_SalesOrder t2 on t1.uGuid1  = t2.uGuid1 \n" +
                "inner join Sys_EmployeeFiles t3 on t2.sSalespersonID = t3.sID \n" +
                "inner join Sal_CustomerFiles scf on t2.sCustomerID = scf.sID \n" +
                "where t1.uGuid1 ='83C5BBD0-54D9-43B7-AF34-00020D156C32'";
        //Query query = entityManager.createNativeQuery(sql, SalSalesOrderEntity.class);
        Query query = entityManager.createNativeQuery(sql);
        //Query query = entityManager.createQuery(sql);
        //query.setParameter("total", "83C5BBD0-54D9-43B7-AF34-00020D156C32");
        //第2种查法：只负责sql执行，不支持动态传参，但可支持条件生成好sql
        //Map<String, Object> stringObjectMap = jdbcTemplate.queryForMap(sql);*/
        //第3种查法：可传参，无需提前定义好接收对象，但需预定义好sql
       /* for (Map<String, Object> getssss : salRepo.getssss()) {
            System.out.println(getssss.get("sPackingMethodName"));
        }*/

        List<String> users = query.getResultList();
        for(String str:users){
            System.out.println(str);
        }
        users.forEach(System.out::println);
        users.forEach(user -> System.out.println(user+","+"111"));




    }
}
