package com.cz.core;

import com.cz.uitl.DesTool;
import org.springframework.security.crypto.password.PasswordEncoder;

public class DesPasswordEncoder implements PasswordEncoder {

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return PasswordEncoder.super.upgradeEncoding(encodedPassword);
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return DesTool.encrypt((String) rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        if (encodedPassword.equals(rawPassword)){
            return true;
        }else{
            return false;
        }
    }
}
