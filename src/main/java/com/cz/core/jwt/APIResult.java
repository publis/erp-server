package com.cz.core.jwt;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class APIResult {

    String code;
    String msg;
    Object data;

    public static APIResult ok(){
        return ok(null);
    }
    public static APIResult ok(Object data){
        return APIResult.builder().code("0").data(data).build();
    }

    public static APIResult error(String msg){
        return APIResult.builder().code("-1").msg(msg).build();
    }
}
