package com.cz.core.user.api;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthUser {
	private String userId;
	private String userName;
	private String dept;
	private String deptName;
	private String position;
	private String positionName;
	private String employeeCode;

}
