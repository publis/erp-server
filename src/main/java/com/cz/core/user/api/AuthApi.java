package com.cz.core.user.api;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.hutool.core.bean.BeanUtil;
import com.cz.uitl.DesTool;
import lombok.extern.slf4j.Slf4j;
import com.cz.core.jwt.APIResult;
import com.cz.core.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cz.core.jwt.JwtTokenUtil;
import com.cz.core.user.User;

@Slf4j
@RestController
public class AuthApi {
	@Autowired AuthenticationManager authManager;
	@Autowired JwtTokenUtil jwtUtil;
	
	@PostMapping("/auth/login")
	public APIResult login(@RequestBody @Valid AuthRequest request, BindingResult bindingResult) {
		try {
			if(bindingResult.hasErrors()){
				return APIResult.error(bindingResult.getAllErrors().get(0).getDefaultMessage());
			}
			Authentication authentication = authManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							request.getAccount(), DesTool.encrypt(request.getPassword()))
			);
			
			User user = (User) authentication.getPrincipal();
			String accessToken = jwtUtil.generateAccessToken(user);
			AuthUser authUser = new AuthUser();
			BeanUtil.copyProperties(user, authUser);
			AuthResponse response = new AuthResponse(authUser, accessToken);

			// return ResponseEntity.ok()
			// 		.header(HttpHeaders.AUTHORIZATION, accessToken)
			// 		.body(response);
			return APIResult.ok(response);
		} catch (BadCredentialsException ex) {
			// return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			return APIResult.error("用户名或密码错误！");
		}
	}

	@Resource
	UserRepository userRepository;

	@GetMapping
	public APIResult findPage(String name, Pageable pageable) {
		log.debug("REST request to get Person by : {}", name);
		User users = new User();
		users.setUserId(name);
		Page<User> page = userRepository.findAll(Example.of(users), pageable);
		return APIResult.ok(page);
	}
}
