package com.cz.core.user.api;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class AuthRequest {
	@NotNull(message = "请输入账号!")
	@Length(min = 5, max = 24)
	private String account;
	
	@NotNull(message = "请输入密码!")
	@Length(min = 6, max = 16)
	private String password;

	private boolean remember;
}
