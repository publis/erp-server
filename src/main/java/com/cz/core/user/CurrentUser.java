package com.cz.core.user;

public class CurrentUser {

    private static final ThreadLocal<User> userThredLocal = new ThreadLocal<User>();

    public static User getUser(){
        return userThredLocal.get();
    }
    public static void setUser(User user){
        userThredLocal.set(user);
    }
    public static void clear(){
        userThredLocal.remove();
    }
}
