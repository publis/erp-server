package com.cz.core.sys.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@Table(name = "SYS_EMPLOYEEFILES")
public class SysEmployeeFiles implements Serializable, Cloneable {

    @Id
    @Column(name = "uguid1")
    private String  guid1;

    @Column(name = "sid")
    private String  id;

    @Column(name = "sname")
    private String  name;

    @Column(name = "sgender")
    private String  gender;

    @Column(name = "smarriage")
    private String  marriage;

    @Column(name = "sphonenumber1")
    private String  phoneNumber1;

    @Column(name = "sphonenumber2")
    private String  phoneNumber2;

    @Column(name = "scompanyid")
    private String  companyID;

    @Column(name = "scompanyname")
    private String  companyName;

    @Column(name = "sdepartmentid")
    private String  departmentID;

    @Column(name = "sdepartmentname")
    private String  departmentName;

    @Column(name = "spositionid")
    private String  positionID;

    @Column(name = "spositionname")
    private String  positionName;

    @Column(name = "bvoid")
    private String  voidFlag;

    @Column(name = "sphoto")
    private String  photo;

    @Column(name = "sjobstatus")
    private String  jobStatus;

    @Column(name = "smailbox")
    private String  mailBox;

    @Column(name = "seducation")
    private String  education;

    @Column(name = "sgraduateschool")
    private String  graduateSchool;

    @Column(name = "spoliticcountenance")
    private String  politicCountenance;

    @Column(name = "sidcard")
    private String  iDCard;

    @Column(name = "tbirthday")
    private Date birthday;

    @Column(name = "snation")
    private String  nation;

    @Column(name = "sprovinceid")
    private String  provinceID;

    @Column(name = "sprovincename")
    private String  provinceName;

    @Column(name = "scityid")
    private String  cityID;

    @Column(name = "scityname")
    private String  cityName;

    @Column(name = "shomeaddress")
    private String  homeAddress;

    @Column(name = "tentrydate")
    private Date entryDate;

    @Column(name = "tquitdate")
    private Date quitDate;

    @Column(name = "dbasesalary")
    private BigDecimal baseSalary;

    @Column(name = "dsubsidy")
    private BigDecimal subsidy;

    @Column(name = "ssubsidyremark")
    private String  subsidyRemark;

    @Column(name = "dmedicalinsurance")
    private BigDecimal medicalInsurance;

    @Column(name = "dmaternityinsurance")
    private BigDecimal maternityInsurance;

    @Column(name = "dinjuryinsurance")
    private BigDecimal injuryInsurance;

    @Column(name = "dunemployinsurance")
    private BigDecimal unEmployInsurance;

    @Column(name = "dpensioninsurance")
    private BigDecimal pensionInsurance;

    @Column(name = "denterpriseannuityinsurance")
    private BigDecimal enterpriseAnnuityInsurance;

    @Column(name = "dgroupaccidentinsurance")
    private BigDecimal groupAccidentInsurance;

    @Column(name = "dgroupmedicalinsurance")
    private BigDecimal groupMedicalInsurance;

    @Column(name = "dgroupillnessinsurance")
    private BigDecimal groupIllnessInsurance;

    @Column(name = "dotherinsurance")
    private BigDecimal otherInsurance;

    @Column(name = "dhouseprovidfund")
    private BigDecimal houseProvidFund;

    @Column(name = "dpensioninsurancefund")
    private BigDecimal pensionInsuranceFund;

    @Column(name = "dmedicainsurancefund")
    private BigDecimal medicaInsuranceFund;

    @Column(name = "dunemployinsurancefund")
    private BigDecimal unemployInsuranceFund;

    @Column(name = "dpublicinjuryinsurancefund")
    private BigDecimal publicInjuryInsuranceFund;

    @Column(name = "dmaternityinsurancefund")
    private BigDecimal maternityInsuranceFund;

    @Column(name = "dsupplypensionfund")
    private BigDecimal supplyPensionFund;

    @Column(name = "dsupplyprovidfund")
    private BigDecimal supplyProvidFund;

    @Column(name = "dotherfund")
    private BigDecimal otherFund;

    @Column(name = "dtargetperformance")
    private BigDecimal targetPerformance;

    @Column(name = "sremark")
    private String  remark;

    @Column(name = "iorder")
    private BigInteger order;

    @Column(name = "bsysflag")
    private String  sysFlag;

    @Column(name = "iindex")
    private BigInteger index;

    @Column(name = "scrtid")
    private String  crtID;

    @Column(name = "scrtname")
    private String  crtName;

    @Column(name = "tcrttime")
    private Date crtTime;

    @Column(name = "smodid")
    private String  modID;

    @Column(name = "smodname")
    private String  modName;

    @Column(name = "tmodtime")
    private Date modTime;

}
