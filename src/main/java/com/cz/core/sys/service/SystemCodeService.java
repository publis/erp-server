package com.cz.core.sys.service;

import cn.hutool.core.util.StrUtil;
import com.cz.core.sys.dao.SystemCodeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SystemCodeService {

    @Resource
    private SystemCodeRepository systemCodeRepository;

    public String getSystemCode(String scId){
        log.info("参数信息:{}" , scId);
        //生成登记编号
        Map map = new HashMap<>();
        map.put("extId","");
        map.put("scId",scId);
        systemCodeRepository.createSystemCode(map);
        if(StrUtil.isBlank(map.get("errNo").toString())){
            return map.get("scCode").toString();
        }else{
            throw new RuntimeException("生成登记单号出错，请重试或者联系管理员！");
        }
    }

}
