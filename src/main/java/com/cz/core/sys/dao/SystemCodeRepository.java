package com.cz.core.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;


public interface SystemCodeRepository extends BaseMapper {

    @Select("{ call Sp_SystemCode(" +
                    " #{map.extId, mode=IN, jdbcType=VARCHAR}," +
                    " #{map.scId, mode=IN, jdbcType=VARCHAR}, " +
                    " #{map.scCode, mode=OUT, jdbcType=VARCHAR}, " +
                    " #{map.errNo, mode=OUT, jdbcType=VARCHAR}) }")
    @Results({
            @Result(column="scCode", property="scCode", jdbcType= JdbcType.VARCHAR),
            @Result(column="errNo", property="errNo", jdbcType= JdbcType.VARCHAR)
    })
    @Options(statementType = StatementType.CALLABLE)
    void createSystemCode(@Param("map") Map map);

}
