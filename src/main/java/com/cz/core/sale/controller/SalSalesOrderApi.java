package com.cz.core.sale.controller;

import com.cz.core.jwt.APIResult;
import com.cz.core.sale.dto.OrderAmountTrendRequest;
import com.cz.core.sale.dto.OrderDeliveryWarnRequest;
import com.cz.core.sale.service.SalSalesOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/sales")
public class SalSalesOrderApi {

	@Autowired
	private SalSalesOrderService salSalesOrderService;
	
	@GetMapping("/deliverywarnlist")
	public APIResult deliveryWarnList(Long current, @Valid OrderDeliveryWarnRequest orderDeliveryWarnRequest, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			return APIResult.error(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining("; ")));
//			return APIResult.error(bindingResult.getAllErrors().get(0).getDefaultMessage());
		}
		return APIResult.ok(salSalesOrderService.getDeliveryWarnList(current, orderDeliveryWarnRequest));
	}

	@GetMapping("/amounttrendlist")
	public APIResult amountTrendList(@Valid OrderAmountTrendRequest orderAmountTrendRequest, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			return APIResult.error(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining("; ")));
		}
		return APIResult.ok(salSalesOrderService.getAmountTrendList(orderAmountTrendRequest));
	}
}
