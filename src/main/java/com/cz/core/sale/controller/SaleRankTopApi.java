package com.cz.core.sale.controller;

import com.cz.core.jwt.APIResult;
import com.cz.core.sale.dto.SaleRankTopRequest;
import com.cz.core.sale.service.SaleRankTopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/sales")
public class SaleRankTopApi {

    @Autowired
    private SaleRankTopService saleRankTopService;

    @GetMapping("/saleranktype")
    public APIResult saleRankTypeList(Long current, @Valid SaleRankTopRequest saleRankTopRequest, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return APIResult.error(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        return APIResult.ok(saleRankTopService.getSaleRankTopList(current,saleRankTopRequest));
    }



}
