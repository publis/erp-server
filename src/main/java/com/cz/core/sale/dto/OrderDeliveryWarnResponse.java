package com.cz.core.sale.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDeliveryWarnResponse implements Serializable, Cloneable {
    private String guid1;
    private String billNo;
    private Date billTime;
    private String customerPO;
    private String contractNo;
    private String customerName;
    private String salespersonName;
    private String merchandiserName;
    private List<OrderDeliveryWarnProduct> productList;
}
