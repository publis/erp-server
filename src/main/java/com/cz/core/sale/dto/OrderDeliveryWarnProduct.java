package com.cz.core.sale.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDeliveryWarnProduct implements Serializable, Cloneable {
    private String guid2;
    private Integer orderID;
    private String productClassName;
    private String productID;
    private String productName;
    private BigDecimal basicQty;
    private String basicUnitName;
    private BigDecimal noDeliveryQty;
    private Date deliveryTime;
    private Integer warningDay;
}
