package com.cz.core.sale.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderAmountTrendRequest {

    @NotNull(message = "请输入开始月份")
    @DateTimeFormat(pattern = "yyyy-MM")
    @JsonFormat(pattern = "yyyy-MM")
    private Date billTimeStart;

    @NotNull(message = "请输入结束月份")
    @DateTimeFormat(pattern = "yyyy-MM")
    @JsonFormat(pattern = "yyyy-MM")
    private Date billTimeEnd;

    private String departmentName;

    private String customerName;

    private String productClassName;

    private String salespersonNam;

    private String merchandiserName;
}
