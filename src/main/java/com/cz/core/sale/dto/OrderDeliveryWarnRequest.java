package com.cz.core.sale.dto;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDeliveryWarnRequest {

    @NotNull(message = "请输入开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date billTimeStart;

    @NotNull(message = "请输入结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date billTimeEnd;

    @NotNull(message = "请输入预警天数")
    private Integer warningDay;

    private String billNo;

    private String customerPO;

    private String contractNo;

    private String customerName;

    private String productID;

    private String productName;

    private String productClassName;

    private String salespersonNam;

    private String merchandiserName;




}
