package com.cz.core.sale.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaleRankTopRespnse implements Serializable, Cloneable{

    @JsonProperty("sGroupDimension")
    private String sGroupDimension;//sGroupDimension

    @JsonProperty("dOMoneyTotal")
    private String dOMoneyTotal;//销售额统计
}
