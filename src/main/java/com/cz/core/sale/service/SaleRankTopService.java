package com.cz.core.sale.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sale.dao.SaleRankTopMapper;
import com.cz.core.sale.dto.SaleRankTopRequest;
import com.cz.core.sale.dto.SaleRankTopRespnse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SaleRankTopService {

    private final Logger logger = LoggerFactory.getLogger(SaleRankTopService.class);

    @Autowired
    private SaleRankTopMapper saleRankTopMapper;

   public Page<SaleRankTopRespnse> getSaleRankTopList(Long currentPage, SaleRankTopRequest saleRankTopRequest){
       Map params = new HashMap();
       BeanUtil.beanToMap(saleRankTopRequest, params, false, false);
       logger.info("销售排行榜参数信息:{}" , saleRankTopRequest);
       return saleRankTopMapper.findSaleRankTopList(new Page<>(currentPage, 20), params);
   }
}
