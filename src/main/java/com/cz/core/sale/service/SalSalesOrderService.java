package com.cz.core.sale.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sale.dao.SalSalesOrderMapper;
import com.cz.core.sale.dto.OrderAmountTrendRequest;
import com.cz.core.sale.dto.OrderAmountTrendResponse;
import com.cz.core.sale.dto.OrderDeliveryWarnRequest;
import com.cz.core.sale.dto.OrderDeliveryWarnResponse;
import com.cz.uitl.JdbcSqlTool;
import com.cz.uitl.PageConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SalSalesOrderService {

    private final Logger logger = LoggerFactory.getLogger(SalSalesOrderService.class);

    @Autowired
    private SalSalesOrderMapper salesOrderMapper;

    public Page<OrderDeliveryWarnResponse> getDeliveryWarnList(Long currentPage, OrderDeliveryWarnRequest orderDeliveryWarnRequest){
        Map params = new HashMap();
        BeanUtil.beanToMap(orderDeliveryWarnRequest, params, false, false);
        logger.info("订单交期预警参数信息:{}" , orderDeliveryWarnRequest);
        return salesOrderMapper.findDeliveryWarnList(new Page<>(currentPage, PageConst.pageSize), params);
    }

    public List<OrderAmountTrendResponse> getAmountTrendList(OrderAmountTrendRequest orderAmountTrendRequest){
        Map params = new HashMap();
        BeanUtil.beanToMap(orderAmountTrendRequest, params, false, false);
        logger.info("销售额趋势参数信息:{}" , orderAmountTrendRequest);
        return salesOrderMapper.findAmountTrendList(params);
    }

}
