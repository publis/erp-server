package com.cz.core.sale.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@Table(name = "SAL_CUSTOMERFILES")
public class SalCustomerFiles implements Serializable, Cloneable {

    @Id
    @Column(name = "uguid1")
    private String  guid1;

    @Column(name = "sid")
    private String  id;

    @Column(name = "sname")
    private String  name;

    @Column(name = "sshortername")
    private String  shorterName;

    @Column(name = "scustomerclassid")
    private String  customerClassID;

    @Column(name = "scustomerclassfullname")
    private String  customerClassFullName;

    @Column(name = "scontact")
    private String  contact;

    @Column(name = "scontactphone")
    private String  contactPhone;

    @Column(name = "smailbox")
    private String  mailBox;

    @Column(name = "sfaxnumber")
    private String  faxNumber;

    @Column(name = "tcooperationtime")
    private Date cooperationTime;

    @Column(name = "sprovinceid")
    private String  provinceID;

    @Column(name = "sprovincename")
    private String  provinceName;

    @Column(name = "scityid")
    private String  cityID;

    @Column(name = "scityname")
    private String  cityName;

    @Column(name = "sshippingaddress")
    private String  shippingAddress;

    @Column(name = "sreceiver")
    private String  receiver;

    @Column(name = "sreceiverphone")
    private String  receiverPhone;

    @Column(name = "scurrencyid")
    private String  currencyID;

    @Column(name = "scurrencyname")
    private String  currencyName;

    @Column(name = "staxid")
    private String  taxID;

    @Column(name = "staxname")
    private String  taxName;

    @Column(name = "itaxrate")
    private BigInteger taxRate;

    @Column(name = "sdeliveryid")
    private String  deliveryID;

    @Column(name = "sdeliveryname")
    private String  deliveryName;

    @Column(name = "smerchandiserid")
    private String  merchandiserID;

    @Column(name = "smerchandisername")
    private String  merchandiserName;

    @Column(name = "ssalespersonid")
    private String  salespersonID;

    @Column(name = "ssalespersonname")
    private String  salespersonName;

    @Column(name = "sidentificationnumber")
    private String  identificationNumber;

    @Column(name = "sbillphone")
    private String  billPhone;

    @Column(name = "sbusinessaddress")
    private String  businessAddress;

    @Column(name = "sbank")
    private String  bank;

    @Column(name = "sbankaccount")
    private String  bankAccount;

    @Column(name = "dreceiptomoneypre")
    private BigDecimal receiptOMoneyPre;

    @Column(name = "sremark")
    private String  remark;

    @Column(name = "bsysflag")
    private String  sysFlag;

    @Column(name = "bvoid")
    private String  voidFlag;

    @Column(name = "iorder")
    private BigInteger order;

    @Column(name = "iindex")
    private BigInteger index;

    @Column(name = "scrtid")
    private String  crtID;

    @Column(name = "scrtname")
    private String  crtName;

    @Column(name = "tcrttime")
    private Date crtTime;

    @Column(name = "smodid")
    private String  modID;

    @Column(name = "smodname")
    private String  modName;

    @Column(name = "tmodtime")
    private Date modTime;

    @Column(name = "sappid")
    private String  appID;

    @Column(name = "sappname")
    private String  appName;

    @Column(name = "tapptime")
    private Date appTime;

    @Column(name = "bapp")
    private String  app;

}
