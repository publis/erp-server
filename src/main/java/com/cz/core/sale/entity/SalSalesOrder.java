package com.cz.core.sale.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@Table(name = "SAL_SALESORDER")
public class SalSalesOrder implements Serializable, Cloneable {

	@Id
	@Column(name = "uguid1")
	private String  guid1;

	@Column(name = "sformtypeid")
	private String  formTypeID;

	@Column(name = "sformtypename")
	private String  formTypeName;

	@Column(name = "sbillno")
	private String  billNo;

	@Column(name = "tbilltime")
	private Date billTime;

	@Column(name = "scustomerid")
	private String  customerID;

	@Column(name = "scustomername")
	private String  customerName;

	@Column(name = "ssalespersonid")
	private String  salespersonID;

	@Column(name = "ssalespersonname")
	private String  salespersonName;

	@Column(name = "smerchandiserid")
	private String  merchandiserID;

	@Column(name = "smerchandisername")
	private String  merchandiserName;

	@Column(name = "scontractno")
	private String  contractNo;

	@Column(name = "scustomerpo")
	private String  customerPO;

	@Column(name = "scontact")
	private String  contact;

	@Column(name = "scontactphone")
	private String  contactPhone;

	@Column(name = "sdeliveryid")
	private String  deliveryID;

	@Column(name = "sdeliveryname")
	private String  deliveryName;

	@Column(name = "tdeliverytime")
	private Date deliveryTime;

	@Column(name = "sshippingaddress")
	private String  shippingAddress;

	@Column(name = "sreceiver")
	private String  receiver;

	@Column(name = "sreceiverphone")
	private String  receiverPhone;

	@Column(name = "scurrencyid")
	private String  currencyID;

	@Column(name = "scurrencyname")
	private String  currencyName;

	@Column(name = "dexchangerate")
	private BigDecimal exchangeRate;

	@Column(name = "sremark")
	private String  remark;

	@Column(name = "bvoid")
	private String  voidFlag;

	@Column(name = "iorder")
	private BigInteger order;

	@Column(name = "iindex")
	private BigInteger index;

	@Column(name = "scrtid")
	private String  crtID;

	@Column(name = "scrtname")
	private String  crtName;

	@Column(name = "tcrttime")
	private Date crtTime;

	@Column(name = "smodid")
	private String  modID;

	@Column(name = "smodname")
	private String  modName;

	@Column(name = "tmodtime")
	private Date modTime;

	@Column(name = "bapp")
	private String  app;

	@Column(name = "sappid")
	private String  appID;

	@Column(name = "sappname")
	private String  appName;

	@Column(name = "tapptime")
	private Date appTime;

}
