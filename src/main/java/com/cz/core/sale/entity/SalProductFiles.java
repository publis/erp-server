package com.cz.core.sale.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@Table(name = "SAL_PRODUCTFILES")
public class SalProductFiles implements Serializable, Cloneable {

    @Id
    @Column(name = "uguid1")
    private String  guid1;

    @Column(name = "sid")
    private String  iD;

    @Column(name = "sname")
    private String  name;

    @Column(name = "sproductsize")
    private String  productSize;

    @Column(name = "ssizetype")
    private String  sizeType;

    @Column(name = "dlength")
    private BigDecimal length;

    @Column(name = "dwidth")
    private BigDecimal width;

    @Column(name = "dheight")
    private BigDecimal height;

    @Column(name = "dvolume")
    private BigDecimal volume;

    @Column(name = "darea")
    private BigDecimal area;

    @Column(name = "dweight")
    private BigDecimal weight;

    @Column(name = "doprice")
    private BigDecimal oPrice;

    @Column(name = "spackingmethodid")
    private String  packingMethodID;

    @Column(name = "spackingmethodname")
    private String  packingMethodName;

    @Column(name = "scustproductid")
    private String  custProductID;

    @Column(name = "sproductclassid")
    private String  productClassID;

    @Column(name = "sproductclassname")
    private String  productClassName;

    @Column(name = "scustomerid")
    private String  customerID;

    @Column(name = "scustomername")
    private String  customerName;

    @Column(name = "sbasicunitid")
    private String  basicUnitID;

    @Column(name = "sbasicunitname")
    private String  basicUnitName;

    @Column(name = "svaluationunitid")
    private String  valuationUnitID;

    @Column(name = "svaluationunitname")
    private String  valuationUnitName;

    @Column(name = "sproductionunitid")
    private String  productionUnitID;

    @Column(name = "sproductionunitname")
    private String  productionUnitName;

    @Column(name = "dvaluconvrate")
    private BigDecimal valuConvRate;

    @Column(name = "dprodconvrate")
    private BigDecimal prodConvRate;

    @Column(name = "tdealtime")
    private Date dealTime;

    @Column(name = "ishelflife")
    private BigInteger shelfLife;

    @Column(name = "dcontainers")
    private BigDecimal containers;

    @Column(name = "sphoto")
    private String  photo;

    @Column(name = "sspecialparam")
    private String  specialParam;

    @Column(name = "sversion")
    private String  version;

    @Column(name = "sboxtype")
    private String  boxType;

    @Column(name = "ipiecestrip")
    private BigInteger pieceStrip;

    @Column(name = "istripbox")
    private BigInteger stripBox;

    @Column(name = "sformat")
    private String  format;

    @Column(name = "ipag")
    private BigInteger pag;

    @Column(name = "dsheet")
    private BigDecimal sheet;

    @Column(name = "sisnb")
    private String  iSNB;

    @Column(name = "ipieceroll")
    private BigInteger pieceRoll;

    @Column(name = "dmetreroll")
    private BigDecimal metreRoll;

    @Column(name = "ipiecesheet")
    private BigInteger pieceSheet;

    @Column(name = "icopybook")
    private BigInteger copyBook;

    @Column(name = "isheets")
    private BigInteger sheets;

    @Column(name = "ssheetdescribe")
    private String  sheetDescribe;

    @Column(name = "bvoid")
    private String  voidFlag;

    @Column(name = "bsysflag")
    private String  sysFlag;

    @Column(name = "bgeneric")
    private String  generic;

    @Column(name = "bsubproduct")
    private String  subProduct;

    @Column(name = "bcheck")
    private String  check;

    @Column(name = "scustomerrequirements")
    private String  customerRequirements;

    @Column(name = "sbindingstyle")
    private String  bindingStyle;

    @Column(name = "sremark")
    private String  remark;

    @Column(name = "iorder")
    private BigInteger order;

    @Column(name = "iindex")
    private BigInteger index;

    @Column(name = "scrtid")
    private String  crtID;

    @Column(name = "scrtname")
    private String  crtName;

    @Column(name = "tcrttime")
    private Date crtTime;

    @Column(name = "smodid")
    private String  modID;

    @Column(name = "smodname")
    private String  modName;

    @Column(name = "tmodtime")
    private Date modTime;

    @Column(name = "bapp")
    private String  app;

    @Column(name = "sappid")
    private String  appID;

    @Column(name = "sappname")
    private String  appName;

    @Column(name = "tapptime")
    private Date appTime;

}
