package com.cz.core.sale.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@Table(name = "SAL_SALESORDER_DETAIL")
public class SalSalesOrderDetail implements Serializable, Cloneable {

	@Column(name = "uguid1")
	private String  guid1;

	@Id
	@Column(name = "uguid2")
	private String  guid2;

	@Column(name = "burgent")
	private String  urgent;

	@Column(name = "bout")
	private String  out;

	@Column(name = "bnoproduction")
	private String  noProduction;

	@Column(name = "bneedproof")
	private String  needProof;

	@Column(name = "sproductiontype")
	private String  productionType;

	@Column(name = "sproductid")
	private String  productID;

	@Column(name = "sproductname")
	private String  productName;

	@Column(name = "sproductsize")
	private String  productSize;

	@Column(name = "sproductclassid")
	private String  productClassID;

	@Column(name = "sproductclassname")
	private String  productClassName;

	@Column(name = "scustproductid")
	private String  custProductID;

	@Column(name = "dbasicqty")
	private BigDecimal basicQty;

	@Column(name = "sbasicunitid")
	private String  basicUnitID;

	@Column(name = "sbasicunitname")
	private String  basicUnitName;

	@Column(name = "dvaluconvrate")
	private BigDecimal valuConvRate;

	@Column(name = "dvaluationqty")
	private BigDecimal valuationQty;

	@Column(name = "svaluationunitid")
	private String  valuationUnitID;

	@Column(name = "svaluationunitname")
	private String  valuationUnitName;

	@Column(name = "staxid")
	private String  taxID;

	@Column(name = "staxname")
	private String  taxName;

	@Column(name = "itaxrate")
	private BigInteger taxRate;

	@Column(name = "doprice")
	private BigDecimal oPrice;

	@Column(name = "domoney")
	private BigDecimal oMoney;

	@Column(name = "dotaxmoney")
	private BigDecimal oTaxMoney;

	@Column(name = "donotaxmoney")
	private BigDecimal oNoTaxMoney;

	@Column(name = "donotaxprice")
	private BigDecimal oNoTaxPrice;

	@Column(name = "doothermoney")
	private BigDecimal oOtherMoney;

	@Column(name = "doamountmoney")
	private BigDecimal oAmountMoney;

	@Column(name = "dbprice")
	private BigDecimal bPrice;

	@Column(name = "dbmoney")
	private BigDecimal bMoney;

	@Column(name = "dbtaxmoney")
	private BigDecimal bTaxMoney;

	@Column(name = "dbnotaxmoney")
	private BigDecimal bNoTaxMoney;

	@Column(name = "dbnotaxprice")
	private BigDecimal bNoTaxPrice;

	@Column(name = "dbothermoney")
	private BigDecimal bOtherMoney;

	@Column(name = "dbamountmoney")
	private BigDecimal bAmountMoney;

	@Column(name = "spackingmethodid")
	private String  packingMethodID;

	@Column(name = "spackingmethodname")
	private String  packingMethodName;

	@Column(name = "dcontainers")
	private BigDecimal containersDouble;

	@Column(name = "icontainers")
	private BigInteger containersInteger;

	@Column(name = "dremnant")
	private BigDecimal remnant;

	@Column(name = "tdeliverytime")
	private Date deliveryTime;

	@Column(name = "scustomerrequirements")
	private String  customerRequirements;

	@Column(name = "bsubproduct")
	private String  subProduct;

	@Column(name = "sremark")
	private String  remark;

	@Column(name = "sversion")
	private String  version;

	@Column(name = "sboxtype")
	private String  boxType;

	@Column(name = "ipiecestrip")
	private BigInteger pieceStrip;

	@Column(name = "istripbox")
	private BigInteger stripBox;

	@Column(name = "sformat")
	private String  format;

	@Column(name = "ipag")
	private BigInteger pag;

	@Column(name = "dsheet")
	private BigDecimal sheet;

	@Column(name = "sisnb")
	private String  iSNB;

	@Column(name = "ipieceroll")
	private BigInteger pieceRoll;

	@Column(name = "dmetreroll")
	private BigDecimal metreRoll;

	@Column(name = "ipiecesheet")
	private BigInteger pieceSheet;

	@Column(name = "icopybook")
	private BigInteger copyBook;

	@Column(name = "isheets")
	private BigInteger sheets;

	@Column(name = "ssheetdescribe")
	private String  sheetDescribe;

	@Column(name = "sserialbegin")
	private String  serialBegin;

	@Column(name = "sserialend")
	private String  serialEnd;

	@Column(name = "dproductionbasicqty")
	private BigDecimal productionBasicQty;

	@Column(name = "bforceproduction")
	private String  forceProduction;

	@Column(name = "dinstorebasicqty")
	private BigDecimal inStoreBasicQty;

	@Column(name = "ddeliverybasicqty")
	private BigDecimal deliveryBasicQty;

	@Column(name = "bforcedelivery")
	private String  forceDelivery;

	@Column(name = "iorder")
	private BigInteger order;

	@Column(name = "dinvoiceomoney")
	private BigDecimal invoiceOMoney;

	@Column(name = "dinvoiceomoneypre")
	private BigDecimal invoiceOMoneyPre;

	@Column(name = "doffsetinvoiceomoneypre")
	private BigDecimal offsetInvoiceOMoneyPre;

	@Column(name = "ddepositomoney")
	private BigDecimal depositOMoney;

	@Column(name = "doffsetdepositomoney")
	private BigDecimal offsetDepositOMoney;

	@Column(name = "bforceout")
	private String  forceOut;

	@Column(name = "doutqty")
	private BigDecimal outQty;

	@Column(name = "doutinqty")
	private BigDecimal outInQty;

	@Column(name = "dreworkqtytotal")
	private BigDecimal reWorkQtyTotal;

	@Column(name = "ddeliverynoticeqty")
	private BigDecimal deliveryNoticeQty;

	@Column(name = "bforcedeliverynotice")
	private String  forceDeliveryNotice;

	@Column(name = "dtransportbasicqty")
	private BigDecimal transportBasicQty;

	@Column(name = "bcheck")
	private String  check;

	@Column(name = "dreportqtytotal")
	private BigDecimal reportQtyTotal;

	@Column(name = "dspecialqtytotal")
	private BigDecimal specialQtyTotal;

	@Column(name = "drejectqtytotal")
	private BigDecimal rejectQtyTotal;

	@Column(name = "dchangeqtytotal")
	private BigDecimal changeQtyTotal;

	@Column(name = "bfirstcheck")
	private String  firstCheck;

	@Column(name = "bforcecheck")
	private String  forceCheck;

	@Column(name = "dproofapplyqty")
	private BigDecimal proofApplyQty;

	@Column(name = "bforceproofapply")
	private String  forceProofApply;

	@Column(name = "dproofqty")
	private BigDecimal proofQty;

	@Column(name = "bforceproof")
	private String  forceProof;

	@Column(name = "doutproofqty")
	private BigDecimal outProofQty;

	@Column(name = "bforceoutproof")
	private String  forceOutProof;

	@Column(name = "sproofresults")
	private String  proofResults;

	@Column(name = "dproofdeliveryqty")
	private BigDecimal proofDeliveryQty;

	@Column(name = "bforceproofdelivery")
	private String  forceProofDelivery;

	@Column(name = "ssourcetype")
	private String  sourceType;

	@Column(name = "ssourceno")
	private String  sourceNo;

	@Column(name = "usourceguid")
	private String  sourceGuid;

	@Column(name = "uproofguid")
	private String  proofGuid;

}
