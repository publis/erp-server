package com.cz.core.sale.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@Table(name = "SAL_PRODUCTFILECLASS")
public class SalProductFileClass implements Serializable, Cloneable {

    @Id
    @Column(name = "uguid1")
    private String  guid1;

    @Column(name = "sid")
    private String  iD;

    @Column(name = "sname")
    private String  name;

    @Column(name = "spid")
    private String  pID;

    @Column(name = "spname")
    private String  pName;

    @Column(name = "sfullname")
    private String  fullName;

    @Column(name = "sbasicunitid")
    private String  basicUnitID;

    @Column(name = "sbasicunitname")
    private String  basicUnitName;

    @Column(name = "svaluationunitid")
    private String  valuationUnitID;

    @Column(name = "svaluationunitname")
    private String  valuationUnitName;

    @Column(name = "sproductionunitid")
    private String  productionUnitID;

    @Column(name = "sproductionunitname")
    private String  productionUnitName;

    @Column(name = "sspecialparam")
    private String  specialParam;

    @Column(name = "spackingmethodid")
    private String  packingMethodID;

    @Column(name = "spackingmethodname")
    private String  packingMethodName;

    @Column(name = "sremark")
    private String  remark;

    @Column(name = "igrade")
    private BigInteger grade;

    @Column(name = "bvoid")
    private String  voidFlag;

    @Column(name = "bsysflag")
    private String  sysFlag;

    @Column(name = "iorder")
    private BigInteger order;

    @Column(name = "iindex")
    private BigInteger index;

    @Column(name = "scrtid")
    private String  crtID;

    @Column(name = "scrtname")
    private String  crtName;

    @Column(name = "tcrttime")
    private Date crtTime;

    @Column(name = "smodid")
    private String  modID;

    @Column(name = "smodname")
    private String  modName;

    @Column(name = "tmodtime")
    private Date modTime;


}
