package com.cz.core.sale.dao;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sale.dto.OrderDeliveryWarnResponse;
import com.cz.core.sale.dto.SaleRankTopRespnse;

import java.util.Map;

public interface SaleRankTopMapper {

    Page<SaleRankTopRespnse> findSaleRankTopList(Page<SaleRankTopRespnse> pg, Map<String, Object> map);
}
