package com.cz.core.sale.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sale.dto.OrderAmountTrendResponse;
import com.cz.core.sale.dto.OrderDeliveryWarnResponse;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;


public interface SalSalesOrderMapper{

    Page<OrderDeliveryWarnResponse> findDeliveryWarnList(Page<OrderDeliveryWarnResponse> pg, @Param("map")  Map<String, Object> map);

    List<OrderAmountTrendResponse> findAmountTrendList(@Param("map") Map<String, Object> map);
}
