package com.cz.core.menu.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.menu.dao.SysLanguageRepository;
import com.cz.core.menu.entity.SysLanguage;
import com.cz.core.sale.dao.SaleRankTopMapper;
import com.cz.core.sale.dto.SaleRankTopRequest;
import com.cz.core.sale.dto.SaleRankTopRespnse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysLanguageService {

    private final Logger logger = LoggerFactory.getLogger(SysLanguageService.class);

    @Autowired
    private SysLanguageRepository sysLanguageRepository;

   public Map<String,Object> getSysLanguage(){
       Map result = new HashMap();
       Map cnLang = new HashMap();
       Map twLang = new HashMap();
       Map enLang = new HashMap();
       final List<SysLanguage> allLanguage = sysLanguageRepository.findAllLanguage();
       for (SysLanguage language :allLanguage){
           cnLang.put(language.getSID(), language.getSLanguageCHS());
           twLang.put(language.getSID(), language.getSLanguageCHT());
           enLang.put(language.getSID(), language.getSLanguageENG());
       }
       result.put("zh_cn", cnLang);
       result.put("zh_tw", twLang);
       result.put("en", enLang);
       return result;
   }
}
