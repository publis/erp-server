package com.cz.core.menu.controller;

import com.cz.core.jwt.APIResult;
import com.cz.core.sale.dto.OrderDeliveryWarnRequest;
import com.cz.core.sale.service.SalSalesOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/sys")
public class SysRoleMenuApi {

	@Autowired
	private SalSalesOrderService salSalesOrderService;
	
	@GetMapping("/onemenu")
	public APIResult getOneMenus() {
		return null;
	}

	@GetMapping("/twomenu")
	public APIResult getTwoMenus() {
		return null;
	}
}
