package com.cz.core.menu.controller;

import com.cz.core.jwt.APIResult;
import com.cz.core.menu.service.SysLanguageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/sys")
public class SysLanguageApi {

	@Autowired
	private SysLanguageService sysLanguageService;
	
	@GetMapping("/language")
	public APIResult getLanguage() {
		return APIResult.ok(sysLanguageService.getSysLanguage());
	}

}
