package com.cz.core.menu.dao;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.menu.entity.SysLanguage;
import com.cz.workorder.dto.ManProductionRegistrationRequest;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


public interface SysLanguageRepository extends BaseMapper<SysLanguage> {
    @Select("select * from Sys_Language t ${ew.customSqlSegment}")
    List<SysLanguage> _findAllLanguage(@Param(Constants.WRAPPER) Wrapper<Map<String,Object>> wrapper);

    default List<SysLanguage> findAllLanguage() {
        QueryWrapper<Map<String,Object>> query = new QueryWrapper<Map<String,Object>>();
        query.likeRight("t.sID", "lyapp");
        return this._findAllLanguage(query);
    }

}
