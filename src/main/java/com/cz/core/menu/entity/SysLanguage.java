package com.cz.core.menu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("SYS_LANGUAGE")
public class SysLanguage implements Serializable, Cloneable {

    @Id
    @TableId
    @Column(name = "uGuid1")
    @TableField("uGuid1")
    private String  uGuid1;

    @Column(name = "sID")
    @TableField("sID")
    private String  sID;

    @Column(name = "sLanguageDefault")
    @TableField("sLanguageDefault")
    private String  sLanguageDefault;

    @Column(name = "sLanguageCHS")
    @TableField("sLanguageCHS")
    private String  sLanguageCHS;

    @Column(name = "sLanguageCHT")
    @TableField("sLanguageCHT")
    private String  sLanguageCHT;

    @Column(name = "sLanguageENG")
    @TableField("sLanguageENG")
    private String  sLanguageENG;

    @Column(name = "sTs")
    @TableField("sTs")
    private String  sTs;

}
