package com.cz.uitl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.bind.DatatypeConverter;
import java.security.spec.KeySpec;
import java.util.Base64;


public class DesTool {

    public static final String secretKey = "quanzhoulvyin1234567890abcdefghijklmnopqrstuvwxyz";
    public static final String ivKey = "1234567890ABCDEF";
    private static final Logger logger = LoggerFactory.getLogger(DesTool.class);

    public static String encrypt(String sources) {
        try {
            KeySpec ks = new DESKeySpec(secretKey.substring(0, 8).getBytes("UTF-8"));
            SecretKey key = SecretKeyFactory.getInstance("DES").generateSecret(ks);
            IvParameterSpec iv = new IvParameterSpec(DatatypeConverter.parseHexBinary(ivKey));
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            byte[] encode = cipher.doFinal(sources.getBytes("UTF-8"));
            return new String(Base64.getEncoder().encode(encode));
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("des 加密出错：", ex);
        }
        return null;
    }

    public static String deEncrypt(String target) {
        try {
            KeySpec ks = new DESKeySpec(secretKey.substring(0, 8).getBytes("UTF-8"));
            SecretKey key = SecretKeyFactory.getInstance("DES").generateSecret(ks);
            IvParameterSpec iv = new IvParameterSpec(DatatypeConverter.parseHexBinary(ivKey));
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            final byte[] decode = Base64.getDecoder().decode(target.getBytes("UTF-8"));
            byte[] decoded = cipher.doFinal(decode);
            return new String(decoded, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("des 解密出错：", ex);
        }
        return null;
    }

}
