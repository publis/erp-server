package com.cz.uitl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class JdbcSqlTool {

    private final Logger logger = LoggerFactory.getLogger(JdbcSqlTool.class);

    public JdbcSqlTool() {

    }

    @Autowired
    private DataSource dataSource;

    private MapLowerRowMapper mapLowerRowMapper = new MapLowerRowMapper();

    public List<Map<String, Object>> queryMapList(String sql, Map<String, Object> parameter) {
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        long startTime = System.currentTimeMillis();
        final List<Map<String, Object>> query = jdbcTemplate.query(sql, parameter, mapLowerRowMapper);
        long endTime = System.currentTimeMillis();
        logger.info("\r\nSQL[{}] \r\n参数[{}] \r\n行数[{}] \r\n执行时间[{}]毫秒", sql, JSONUtil.toJsonStr(parameter), query.size(), endTime - startTime);
        return query;
    }

    public List<Map<String, Object>> queryMapList(String sql, Object obj) {
        Map<String, Object> parameter = new LinkedHashMap<>();
        BeanUtil.beanToMap(obj, parameter, false, false);
        return queryMapList(sql, parameter);
    }

    public List<Map<String, Object>> queryMapList(String sql) {
        return queryMapList(sql, new HashMap<String, Object>());
    }


}
