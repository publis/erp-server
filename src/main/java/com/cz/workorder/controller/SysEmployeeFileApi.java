package com.cz.workorder.controller;

import com.cz.core.jwt.APIResult;
import com.cz.workorder.dto.SysEmployeeFileRequest;
import com.cz.workorder.service.SysEmployeeFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/sysemployee")
public class SysEmployeeFileApi {

	@Autowired
	private SysEmployeeFileService service;


	@GetMapping("/searchemployee")
	public APIResult findPage(Long current, SysEmployeeFileRequest params) {
		return APIResult.ok(service.searchEmployee(current, params));
	}

}
