package com.cz.workorder.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.jwt.APIResult;
import com.cz.core.user.CurrentUser;
import com.cz.core.user.User;
import com.cz.workorder.WorkOrderRepository;
import com.cz.workorder.dao.ManProductionRegistrationRepository;
import com.cz.workorder.dto.ManProductionRegistrationRequest;
import com.cz.workorder.dto.ProductionTaskRequest;
import com.cz.workorder.dto.WorkOrderListReq;
import com.cz.workorder.service.ManProductionRegistrationService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/taskorder")
public class ManProductionRegistrationApi {

	@Autowired
	private ManProductionRegistrationService service;


	@GetMapping("/findlist")
	public APIResult findPage(Long current,  @Valid ManProductionRegistrationRequest params, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			return APIResult.error(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining("; ")));
		}
		return APIResult.ok(service.getAmountTrendList(current, params));
	}

	@GetMapping("/findtodotask")
	public APIResult findTodoTask(Long current, String sBillNo) {
		return APIResult.ok(service.getTodoTaskList(current, sBillNo));
	}

	@GetMapping("/ptoductiontask")
	public APIResult getProductionTask(String uBarCode) {
		return APIResult.ok(service.getProductionTask(uBarCode));
	}

	@GetMapping("/findguid")
	public APIResult getProductionByGuid(String uGuid1, String uGuid2) {
		return APIResult.ok(service.getProductionByGuid(uGuid1, uGuid2));
	}

	@PostMapping("/save")
	@Transactional
	public APIResult save(@RequestBody @Valid  ProductionTaskRequest param, BindingResult bindingResult) {
		try{
			if(bindingResult.hasErrors()){
				return APIResult.error(bindingResult.getAllErrors().get(0).getDefaultMessage());
			}
			return APIResult.ok(service.save(param));
		}catch (Exception ex){
			log.error("任务报工保存出错！",ex);
			return APIResult.error(ex.getMessage());
		}
	}

	@GetMapping("/delete")
	@Transactional
	public APIResult delete(String uGuid1) {
		try{
			service.delete(uGuid1);
		}catch (Exception ex){
			log.error("任务报工删除失败！",ex);
			return APIResult.error(ex.getMessage());
		}
		return APIResult.ok();
	}

	@GetMapping("/audi")
	@Transactional
	public APIResult audi(String uGuid1) {
		try{
			service.audi(uGuid1);
		}catch (Exception ex){
			log.error("任务报工审核失败！",ex);
			return APIResult.error(ex.getMessage());
		}
		return APIResult.ok();
	}

	@GetMapping("/cancelaudi")
	@Transactional
	public APIResult cancelAudi(String uGuid1) {
		try{
			service.cancelAudi(uGuid1);
		}catch (Exception ex){
			log.error("任务报工取消审核失败！",ex);
			return APIResult.error(ex.getMessage());
		}
		return APIResult.ok();
	}

}
