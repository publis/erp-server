package com.cz.workorder.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.sys.service.SystemCodeService;
import com.cz.core.user.CurrentUser;
import com.cz.core.user.User;
import com.cz.uitl.PageConst;
import com.cz.workorder.dao.ManProductionRegistrationDetailEmployeeRepository;
import com.cz.workorder.dao.ManProductionRegistrationDetailRepository;
import com.cz.workorder.dao.ManProductionRegistrationRepository;
import com.cz.workorder.dao.ManWorkOrderRepository;
import com.cz.workorder.dto.ManProductionRegistrationRequest;
import com.cz.workorder.dto.ProductionTaskRequest;
import com.cz.workorder.dto.TaskWorkOrderInfoReq;
import com.cz.workorder.entity.ManProductionRegistration;
import com.cz.workorder.entity.ManProductionRegistrationDetail;
import com.cz.workorder.entity.ManProductionRegistrationDetailEmployee;
import com.cz.workorder.entity.ManWorkOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ManProductionRegistrationService {

    private final Logger logger = LoggerFactory.getLogger(ManProductionRegistrationService.class);

    @Resource
    private ManProductionRegistrationRepository repository;

    @Resource
    private ManProductionRegistrationDetailRepository repositoryDetail;

    @Resource
    private ManProductionRegistrationDetailEmployeeRepository repositoryDetailEmploy;

    @Autowired
    private ManWorkOrderRepository ManWorkOrderRepository;

    @Resource
    private SystemCodeService systemCodeService;


    public Page<Map<String, Object>> getAmountTrendList(Long currentPage, ManProductionRegistrationRequest registrationRequest) {
        logger.info("参数信息:{}", registrationRequest);
        return repository.findPage(new Page<>(currentPage, PageConst.pageSize), registrationRequest);
    }


    public Page<Map<String, Object>> getTodoTaskList(Long currentPage, String billNo) {
        logger.info("参数信息:{}", billNo);
        return repository.findByTodoTask(new Page<>(currentPage, PageConst.pageSize), billNo);
    }

    public Map<String, Object> getProductionTask(String uBarCode) {
        logger.info("参数信息:{}", uBarCode);
        return repository.findByProductionTask(uBarCode);
    }

    public TaskWorkOrderInfoReq getProductionByGuid(String uGuid1, String uGuid2) {
        logger.info("参数信息:{} {}", uGuid1, uGuid2);
        TaskWorkOrderInfoReq res = new TaskWorkOrderInfoReq();
        ManProductionRegistration manProductionRegistration = repository.selectById(uGuid1);
        if("1".equals(manProductionRegistration.getSCountType())){
            manProductionRegistration.setSCountTypeName("按班组计数");
        }else{
            manProductionRegistration.setSCountType("2");
            manProductionRegistration.setSCountTypeName("按员工计数");
        }
        ManProductionRegistrationDetail manProductionRegistrationDetail = repositoryDetail.selectById(uGuid2);
        Map<String, Object> columnMap = new HashMap<>();
        columnMap.put("uGuid1", uGuid1);
        columnMap.put("uGuid2", uGuid2);
        List<ManProductionRegistrationDetailEmployee> manProductionRegistrationDetailEmployees = repositoryDetailEmploy.selectByMap(columnMap);
        res.setRegistration(manProductionRegistration);
        res.setDetail(manProductionRegistrationDetail);
        res.setEmployeeList(manProductionRegistrationDetailEmployees);
        return res;
    }

    public Map<String, Object> save(ProductionTaskRequest productionTask) {
        final User user = CurrentUser.getUser();
        logger.info("参数信息:{}", productionTask);

        // Man_ProductionRegistration
        ManProductionRegistration productionRegistration = new ManProductionRegistration();
        BeanUtils.copyProperties(productionTask, productionRegistration);
        String guuid1 = StrUtil.uuid();
        productionRegistration.setUGuid1(guuid1);
        productionRegistration.setSReportSource("APP02");
        productionRegistration.setTBillTime(DateUtil.date());
        productionRegistration.setSEmployeeID(user.getEmployeeCode());
        productionRegistration.setSEmployeeName(user.getUserName());
        productionRegistration.setSCrtID(user.getUserId());
        productionRegistration.setSCrtName(user.getUserName());
        productionRegistration.setTCrtTime(DateUtil.date());
        productionRegistration.setBVoid("0");
        productionRegistration.setBApp("0");
        productionRegistration.setSBillNo(systemCodeService.getSystemCode("CAppProductionRegistration"));
        repository.insert(productionRegistration);

        //Man_ProductionRegistration_Detail
        ManProductionRegistrationDetail detail = new ManProductionRegistrationDetail();
        BeanUtils.copyProperties(productionTask, detail);
        detail.setUGuid1(guuid1);
        String guuid2 = StrUtil.uuid();
        detail.setUGuid2(guuid2);
        detail.setIOrder(1);
        repositoryDetail.insert(detail);

        //Man_ProductionRegistration_Detail_Employee
        productionTask.getUserTask().stream().forEach(item -> {
            ManProductionRegistrationDetailEmployee detailEmployee = new ManProductionRegistrationDetailEmployee();
            BeanUtils.copyProperties(item, detailEmployee);
            detailEmployee.setUGuid1(guuid1);
            detailEmployee.setUGuid2(guuid2);
            detailEmployee.setUGuid3(StrUtil.uuid());
            repositoryDetailEmploy.insert(detailEmployee);
        });

        Map<String, Object> res = new HashMap<>();
        res.put("uGuid1", guuid1);
        res.put("uGuid2", guuid2);
        return res;
    }

    public void delete(String uGuid1) {
        logger.info("参数信息:{}", uGuid1);
        // Man_ProductionRegistration
        Map param = new HashMap<>();
        param.put("uGuid1", uGuid1);
        repository.deleteByMap(param);
        //Man_ProductionRegistration_Detail
        repositoryDetail.deleteByMap(param);
        //Man_ProductionRegistration_Detail_Employee
        repositoryDetailEmploy.deleteByMap(param);
    }

    public void audi(String uGuid1) {
        logger.info("参数信息:{}", uGuid1);
        final User user = CurrentUser.getUser();
        // Man_ProductionRegistration
        ManProductionRegistration productionRegistration = new ManProductionRegistration();
        productionRegistration.setUGuid1(uGuid1);
        productionRegistration.setBApp("1");
        productionRegistration.setSAppID(user.getEmployeeCode());
        productionRegistration.setSAppName(user.getUserName());
        productionRegistration.setTAppTime(DateUtil.date());
        repository.updateById(productionRegistration);
    }

    public void cancelAudi(String uGuid1) {
        logger.info("参数信息:{}", uGuid1);
        ManProductionRegistration productionRegistration = new ManProductionRegistration();
        productionRegistration.setUGuid1(uGuid1);
        ManProductionRegistration manProductionRegistration = repository.selectById(productionRegistration);
        final List<ManWorkOrder> manWorkOrderList = ManWorkOrderRepository.selectByMap(new HashMap<String, Object>() {{
            put("sBillNo", manProductionRegistration.getSBillNo());
        }});
        if(manWorkOrderList.size()>1 && manWorkOrderList.get(0).getBComplete().equals("1")){
            throw new RuntimeException("对于已经完工的工单，不允许再做消审动作!");
        }

        repository.update(
                null,
                Wrappers.<ManProductionRegistration>update()
                        .set("bApp", "0")
                        .set("sAppID", null)
                        .set("sAppName", null)
                        .set("tAppTime", null)
                        .eq("uGuid1", uGuid1)
        );
    }

}
