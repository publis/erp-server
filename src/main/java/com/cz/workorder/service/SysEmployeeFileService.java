package com.cz.workorder.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.uitl.PageConst;
import com.cz.workorder.dao.SysEmployeeFileRepository;
import com.cz.workorder.dto.ManProductionRegistrationRequest;
import com.cz.workorder.dto.SysEmployeeFileRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class SysEmployeeFileService {

    private final Logger logger = LoggerFactory.getLogger(SysEmployeeFileService.class);

    @Resource
    private SysEmployeeFileRepository repository;

    public Page<Map<String,Object>> searchEmployee(Long currentPage, SysEmployeeFileRequest param){
        logger.info("参数信息:{}" , param);
        return repository.searchEmployee(new Page<>(currentPage, PageConst.pageSize), param);
    }

}
