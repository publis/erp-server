package com.cz.workorder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Table(name = "View_Man_ProductionRegistration")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderDTO {
    @Id
    String sBillNo;
    String tBillTime;
    String sReportSource;
    String sWorkCenterName;
    String sMachineName;
    String sEmployeeName;
    String sWorkOrderNo;
    String sCustomerName;
    String sProductName;
    String sProcessName;
    String dQty;
    String dOutPutQty;
    String dWasteQty;
}
