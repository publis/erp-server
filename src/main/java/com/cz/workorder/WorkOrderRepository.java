package com.cz.workorder;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.workorder.dto.WorkOrderListReq;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.util.StringUtils;

import java.util.Map;


public interface WorkOrderRepository extends BaseMapper {

    @Select("select * from View_Man_ProductionRegistration pr ${ew.customSqlSegment}")
    Page<Map<String, Object>> _findByPages(Page page, @Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

    @Select("select * from View_Man_ProductionRegistration pr ${ew.customSqlSegment}")
    Map<String, Object> _findByObject(@Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

    default Page<Map<String, Object>> findPage(Page page, WorkOrderListReq params) {
        LambdaQueryWrapper<Map<String, Object>> query = new QueryWrapper<Map<String, Object>>().lambda();
        query.apply("pr.tBillTime between {0} and {1}", params.getTBillTimeStart(), params.getTBillTimeEnd());
        if (StringUtils.hasText(params.getBAPP()) && Integer.parseInt(params.getBAPP()) > -1) {
            query.apply("pr.bAPP={0}", params.getBAPP());
        }
        if (StringUtils.hasText(params.getSBillNo())) {
            query.apply("pr.sBillNo like {0}", "%" + params.getSBillNo() + "%");
        }
        if (StringUtils.hasText(params.getSWorkOrderNo())) {
            query.apply("pr.sWorkOrderNo like {0}", "%" + params.getSWorkOrderNo() + "%");
        }
        if (StringUtils.hasText(params.getUBarCode())) {
            query.apply("pr.uBarCode={0}", params.getUBarCode());
        }
        if (StringUtils.hasText(params.getSWorkCenterName())) {
            query.apply("pr.sWorkCenterName={0}", params.getSWorkCenterName());
        }
        if (StringUtils.hasText(params.getSMachineName())) {
            query.apply("pr.sMachineName={0}", params.getSMachineName());
        }
        if (StringUtils.hasText(params.getSTeamName())) {
            query.apply("pr.sTeamName={0}", params.getSTeamName());
        }
        if (StringUtils.hasText(params.getSProductID())) {
            query.apply("pr.sProductID={0}", params.getSProductID());
        }
        if (StringUtils.hasText(params.getSProductName())) {
            query.apply("pr.sProductName={0}", params.getSProductName());
        }
        return this._findByPages(page, query);
    }

    default Map<String, Object> findByGuid(String guid) {
        LambdaQueryWrapper<Map<String, Object>> query = new QueryWrapper<Map<String, Object>>().lambda();
        query.apply("pr.uGuid2={0}", guid);
        return this._findByObject(query);
    }


    @Select(" SELECT * FROM View_WorkOrderTodoList('1990-01-01', '2999-12-31', '', '', '', '', '', '', 1)  " +
            " ${ew.customSqlSegment}")
    Page<Map<String, Object>> _findByTodo(Page page, @Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

    default Page<Map<String, Object>> findByTodo(Page page, String sBillNo) {
        LambdaQueryWrapper<Map<String, Object>> query = new QueryWrapper<Map<String, Object>>().lambda();
        if (StringUtils.hasText(sBillNo)) {
            query.apply("sBillNo like {0}","%" +sBillNo + "%" );
        }
        return this._findByTodo(page, query);
    }
}
