package com.cz.workorder.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("MAN_PRODUCTIONREGISTRATION")
public class ManProductionRegistration implements Serializable, Cloneable {

    @Id
    @TableId
    @Column(name = "uGuid1")
    @TableField("uGuid1")
    @JsonProperty("uGuid1")
    private String  uGuid1;

    @Column(name = "sBillNo")
    @TableField("sBillNo")
    @JsonProperty("sBillNo")
    private String  sBillNo;

    @Column(name = "tBillTime")
    @TableField("tBillTime")
    @JsonProperty("tBillTime")
    private Date tBillTime;

    @Column(name = "sReportSource")
    @TableField("sReportSource")
    @JsonProperty("sReportSource")
    private String  sReportSource;

    @Column(name = "sWorkCenterID")
    @TableField("sWorkCenterID")
    @JsonProperty("sWorkCenterID")
    private String  sWorkCenterID;

    @Column(name = "sWorkCenterName")
    @TableField("sWorkCenterName")
    @JsonProperty("sWorkCenterName")
    private String  sWorkCenterName;

    @Column(name = "sMachineID")
    @TableField("sMachineID")
    @JsonProperty("sMachineID")
    private String  sMachineID;

    @Column(name = "sMachineName")
    @TableField("sMachineName")
    @JsonProperty("sMachineName")
    private String  sMachineName;

    @Column(name = "sCountType")
    @TableField("sCountType")
    @JsonProperty("sCountType")
    private String  sCountType;

    @TableField(exist = false)
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty("sCountTypeName")
    private String  sCountTypeName;

    @Column(name = "sEmployeeID")
    @TableField("sEmployeeID")
    @JsonProperty("sEmployeeID")
    private String  sEmployeeID;

    @Column(name = "sEmployeeName")
    @TableField("sEmployeeName")
    @JsonProperty("sEmployeeName")
    private String  sEmployeeName;

    @Column(name = "bVoid")
    @TableField("bVoid")
    @JsonProperty("bVoid")
    private String  bVoid;

    @Column(name = "sRemark")
    @TableField("sRemark")
    @JsonProperty("sRemark")
    private String  sRemark;

    @Column(name = "iOrder")
    @TableField("iOrder")
    @JsonProperty("iOrder")
    private Integer iOrder;

    @Column(name = "iIndex")
    @TableField("iIndex")
    @JsonProperty("iIndex")
    private Integer iIndex;

    @Column(name = "sCrtID")
    @TableField("sCrtID")
    @JsonProperty("sCrtID")
    private String  sCrtID;

    @Column(name = "sCrtName")
    @TableField("sCrtName")
    @JsonProperty("sCrtName")
    private String  sCrtName;

    @Column(name = "tCrtTime")
    @TableField("tCrtTime")
    @JsonProperty("tCrtTime")
    private Date tCrtTime;

    @Column(name = "sModID")
    @TableField("sModID")
    @JsonProperty("sModID")
    private String  sModID;

    @Column(name = "sModName")
    @TableField("sModName")
    @JsonProperty("sModName")
    private String  sModName;

    @Column(name = "tModTime")
    @TableField("tModTime")
    @JsonProperty("tModTime")
    private Date tModTime;

    @Column(name = "bApp")
    @TableField("bApp")
    @JsonProperty("bApp")
    private String  bApp;

    @Column(name = "sAppID")
    @TableField("sAppID")
    @JsonProperty("sAppID")
    private String  sAppID;

    @Column(name = "sAppName")
    @TableField("sAppName")
    @JsonProperty("sAppName")
    private String  sAppName;

    @Column(name = "tAppTime")
    @TableField("tAppTime")
    @JsonProperty("tAppTime")
    private Date tAppTime;

    @Column(name = "sTs")
    @TableField("sTs")
    @JsonProperty("sTs")
    private String  sTs;

    @Column(name = "sTeamID")
    @TableField("sTeamID")
    @JsonProperty("sTeamID")
    private String  sTeamID;

    @Column(name = "sTeamName")
    @TableField("sTeamName")
    @JsonProperty("sTeamName")
    private String  sTeamName;
}
