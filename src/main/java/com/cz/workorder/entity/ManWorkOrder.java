package com.cz.workorder.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("MAN_WORKORDER")
public class ManWorkOrder implements Serializable, Cloneable {

    @Id
    @TableId
    @Column(name = "uGuid1")
    @TableField("uGuid1")
    private String  uGuid1;

    @Column(name = "sBillNo")
    @TableField("sBillNo")
    private String  sBillNo;

    @Column(name = "tBillTime")
    @TableField("tBillTime")
    private Date tBillTime;

    @Column(name = "sBillType")
    @TableField("sBillType")
    private String  sBillType;

    @Column(name = "sWorkOrderType")
    @TableField("sWorkOrderType")
    private String  sWorkOrderType;

    @Column(name = "sBOMName")
    @TableField("sBOMName")
    private String  sBOMName;

    @Column(name = "sBOMID")
    @TableField("sBOMID")
    private String  sBOMID;

    @Column(name = "bUrgent")
    @TableField("bUrgent")
    private String  bUrgent;

    @Column(name = "bOut")
    @TableField("bOut")
    private String  bOut;

    @Column(name = "bComplete")
    @TableField("bComplete")
    private String  bComplete;

    @Column(name = "tCompleteTime")
    @TableField("tCompleteTime")
    private Date tCompleteTime;

    @Column(name = "bVoid")
    @TableField("bVoid")
    private String  bVoid;

    @Column(name = "sLongStrProductName")
    @TableField("sLongStrProductName")
    private String  sLongStrProductName;

    @Column(name = "sRemark")
    @TableField("sRemark")
    private String  sRemark;

    @Column(name = "iOrder")
    @TableField("iOrder")
    private Integer O;

    @Column(name = "iIndex")
    @TableField("iIndex")
    private Integer I;

    @Column(name = "sCrtID")
    @TableField("sCrtID")
    private String  sCrtID;

    @Column(name = "sCrtName")
    @TableField("sCrtName")
    private String  sCrtName;

    @Column(name = "tCrtTime")
    @TableField("tCrtTime")
    private Date tCrtTime;

    @Column(name = "sModID")
    @TableField("sModID")
    private String  sModID;

    @Column(name = "sModName")
    @TableField("sModName")
    private String  sModName;

    @Column(name = "tModTime")
    @TableField("tModTime")
    private Date tModTime;

    @Column(name = "bApp")
    @TableField("bApp")
    private String  bApp;

    @Column(name = "sAppID")
    @TableField("sAppID")
    private String  sAppID;

    @Column(name = "sAppName")
    @TableField("sAppName")
    private String  sAppName;

    @Column(name = "tAppTime")
    @TableField("tAppTime")
    private Date tAppTime;

    @Column(name = "sProcessingType")
    @TableField("sProcessingType")
    private String  sProcessingType;

    @Column(name = "sSupplierID")
    @TableField("sSupplierID")
    private String  sSupplierID;

    @Column(name = "sSupplierName")
    @TableField("sSupplierName")
    private String  sSupplierName;

    @Column(name = "sContact")
    @TableField("sContact")
    private String  sContact;

    @Column(name = "sContactPhone")
    @TableField("sContactPhone")
    private String  sContactPhone;

    @Column(name = "sPurchaserDepartName")
    @TableField("sPurchaserDepartName")
    private String  sPurchaserDepartName;

    @Column(name = "sPurchaserDepartID")
    @TableField("sPurchaserDepartID")
    private String  sPurchaserDepartID;

    @Column(name = "sPurchaserName")
    @TableField("sPurchaserName")
    private String  sPurchaserName;

    @Column(name = "sPurchaserID")
    @TableField("sPurchaserID")
    private String  sPurchaserID;

    @Column(name = "sRequestDepartName")
    @TableField("sRequestDepartName")
    private String  sRequestDepartName;

    @Column(name = "sRequestDepartID")
    @TableField("sRequestDepartID")
    private String  sRequestDepartID;

    @Column(name = "sRequesterName")
    @TableField("sRequesterName")
    private String  sRequesterName;

    @Column(name = "sRequesterID")
    @TableField("sRequesterID")
    private String  sRequesterID;

    @Column(name = "tDeliveryTime")
    @TableField("tDeliveryTime")
    private Date tDeliveryTime;

    @Column(name = "sContractNo")
    @TableField("sContractNo")
    private String  sContractNo;

    @Column(name = "sDeliveryName")
    @TableField("sDeliveryName")
    private String  sDeliveryName;

    @Column(name = "sDeliveryID")
    @TableField("sDeliveryID")
    private String  sDeliveryID;

    @Column(name = "sCurrencyName")
    @TableField("sCurrencyName")
    private String  sCurrencyName;

    @Column(name = "sCurrencyID")
    @TableField("sCurrencyID")
    private String  sCurrencyID;

    @Column(name = "dExchangeRate")
    @TableField("dExchangeRate")
    private Double E;

    @Column(name = "sTs")
    @TableField("sTs")
    private String  sTs;

}
