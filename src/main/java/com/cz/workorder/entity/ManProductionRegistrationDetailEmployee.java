package com.cz.workorder.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("MAN_PRODUCTIONREGISTRATION_DETAIL_EMPLOYEE")
public class ManProductionRegistrationDetailEmployee implements Serializable, Cloneable {

    @Column(name = "uGuid1")
    @TableField("uGuid1")
    @JsonProperty("uGuid1")
    private String  uGuid1;

    @Column(name = "uGuid2")
    @TableField("uGuid2")
    @JsonProperty("uGuid2")
    private String  uGuid2;

    @Id
    @TableId
    @Column(name = "uGuid3")
    @TableField("uGuid3")
    @JsonProperty("uGuid3")
    private String  uGuid3;

    @Column(name = "iOrder")
    @TableField("iOrder")
    @JsonProperty("iOrder")
    private Integer iOrder;

    @Column(name = "sEmployeeID")
    @TableField("sEmployeeID")
    @JsonProperty("sEmployeeID")
    private String  sEmployeeID;

    @Column(name = "sEmployeeName")
    @TableField("sEmployeeName")
    @JsonProperty("sEmployeeName")
    private String  sEmployeeName;

    @Column(name = "sDepartmentID")
    @TableField("sDepartmentID")
    @JsonProperty("sDepartmentID")
    private String  sDepartmentID;

    @Column(name = "sDepartmentName")
    @TableField("sDepartmentName")
    @JsonProperty("sDepartmentName")
    private String  sDepartmentName;

    @Column(name = "sPositionID")
    @TableField("sPositionID")
    @JsonProperty("sPositionID")
    private String  sPositionID;

    @Column(name = "sPositionName")
    @TableField("sPositionName")
    @JsonProperty("sPositionName")
    private String  sPositionName;

    @Column(name = "dQty")
    @TableField("dQty")
    @JsonProperty("dQty")
    private Double dQty;

    @Column(name = "dWasteQty")
    @TableField("dWasteQty")
    @JsonProperty("dWasteQty")
    private Double dWasteQty;

    @Column(name = "dOutPutQty")
    @TableField("dOutPutQty")
    @JsonProperty("dOutPutQty")
    private Double dOutPutQty;

    @Column(name = "sRemark")
    @TableField("sRemark")
    @JsonProperty("sRemark")
    private String  sRemark;

    @Column(name = "sTs")
    @TableField("sTs")
    @JsonProperty("sTs")
    private String  sTs;

}
