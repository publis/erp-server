package com.cz.workorder.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("VIEW_MAN_PRODUCTIONREGISTRATION")
public class ManProductionRegistrationView implements Serializable, Cloneable {

    @Column(name = "sbillno")
    private String  billNo;

    @Column(name = "tbilltime")
    private Date billTime;

    @Column(name = "sreportsource")
    private String  reportSource;

    @Column(name = "sworkcentername")
    private String  workCenterName;

    @Column(name = "smachinename")
    private String  machineName;

    @Column(name = "semployeename")
    private String  employeeName;

    @Column(name = "sworkorderno")
    private String  workOrderNo;

    @Column(name = "scustomername")
    private String  customerName;

    @Column(name = "sproductid")
    private String  productID;

    @Column(name = "sproductname")
    private String  productName;

    @Column(name = "sproductsize")
    private String  productSize;

    @Column(name = "dproductqty")
    private BigDecimal productQty;

    @Column(name = "spartname")
    private String  partName;

    @Column(name = "sprocessname")
    private String  processName;

    @Column(name = "sproductionunitname")
    private String  productionUnitName;

    @Column(name = "dprocessqty")
    private BigDecimal processQty;

    @Column(name = "dqty")
    private BigDecimal qty;

    @Column(name = "dwasteqty")
    private BigDecimal wasteQty;

    @Column(name = "doutputqty")
    private BigDecimal outPutQty;

    @Column(name = "sprintmethod")
    private String  printMethod;

    @Column(name = "icolornumber")
    private BigInteger colorNumber;

    @Column(name = "imodulus")
    private BigInteger modulus;

    @Column(name = "icutnumber")
    private BigInteger cutNumber;

    @Column(name = "dcutlength")
    private BigDecimal cutLength;

    @Column(name = "dcutwidth")
    private BigDecimal cutWidth;

    @Column(name = "ssalesorderno")
    private String  salesOrderNo;

    @Column(name = "scustomerpo")
    private String  customerPO;

    @Column(name = "sremark")
    private String  remark;

    @Column(name = "scrtid")
    private String  crtID;

    @Column(name = "scrtname")
    private String  crtName;

    @Column(name = "tcrttime")
    private Date crtTime;

    @Column(name = "smodid")
    private String  modID;

    @Column(name = "smodname")
    private String  modName;

    @Column(name = "tmodtime")
    private Date modTime;

    @Column(name = "bvoid")
    private String  voidFlag;

    @Column(name = "bapp")
    private String  app;

    @Column(name = "sappid")
    private String  appID;

    @Column(name = "sappname")
    private String  appName;

    @Column(name = "tapptime")
    private Date appTime;

    @Column(name = "uguid1")
    private String  guid1;

    @Id
    @Column(name = "uguid2")
    private String  guid2;

    @Column(name = "ssourcetype")
    private String  sourceType;

}
