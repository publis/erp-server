package com.cz.workorder.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("MAN_PRODUCTIONREGISTRATION_DETAIL")
public class ManProductionRegistrationDetail implements Serializable, Cloneable {

    @Column(name = "uGuid1")
    @TableField("uGuid1")
    @JsonProperty("uGuid1")
    private String  uGuid1;

    @Id
    @TableId
    @Column(name = "uGuid2")
    @TableField("uGuid2")
    @JsonProperty("uGuid2")
    private String  uGuid2;

    @Column(name = "iOrder")
    @TableField("iOrder")
    @JsonProperty("iOrder")
    private Integer iOrder;

    @Column(name = "sSourceType")
    @TableField("sSourceType")
    @JsonProperty("sSourceType")
    private String  sSourceType;

    @Column(name = "sWorkOrderNo")
    @TableField("sWorkOrderNo")
    @JsonProperty("sWorkOrderNo")
    private String  sWorkOrderNo;

    @Column(name = "sCustomerID")
    @TableField("sCustomerID")
    @JsonProperty("sCustomerID")
    private String  sCustomerID;

    @Column(name = "sCustomerName")
    @TableField("sCustomerName")
    @JsonProperty("sCustomerName")
    private String  sCustomerName;

    @Column(name = "sProductID")
    @TableField("sProductID")
    @JsonProperty("sProductID")
    private String  sProductID;

    @Column(name = "sProductName")
    @TableField("sProductName")
    @JsonProperty("sProductName")
    private String  sProductName;

    @Column(name = "sProductSize")
    @TableField("sProductSize")
    @JsonProperty("sProductSize")
    private String  sProductSize;

    @Column(name = "dProductQty")
    @TableField("dProductQty")
    @JsonProperty("dProductQty")
    private Double dProductQty;

    @Column(name = "sPartName")
    @TableField("sPartName")
    @JsonProperty("sPartName")
    private String  sPartName;

    @Column(name = "sProcessID")
    @TableField("sProcessID")
    @JsonProperty("sProcessID")
    private String  sProcessID;

    @Column(name = "sProcessName")
    @TableField("sProcessName")
    @JsonProperty("sProcessName")
    private String  sProcessName;

    @Column(name = "dProcessQty")
    @TableField("dProcessQty")
    @JsonProperty("dProcessQty")
    private Double dProcessQty;

    @Column(name = "sProductionUnitID")
    @TableField("sProductionUnitID")
    @JsonProperty("sProductionUnitID")
    private String  sProductionUnitID;

    @Column(name = "sProductionUnitName")
    @TableField("sProductionUnitName")
    @JsonProperty("sProductionUnitName")
    private String  sProductionUnitName;

    @Column(name = "dRegistQty")
    @TableField("dRegistQty")
    @JsonProperty("dRegistQty")
    private Double dRegistQty;

    @Column(name = "dNoReportQty")
    @TableField("dNoReportQty")
    @JsonProperty("dNoReportQty")
    private Double dNoReportQty;

    @Column(name = "dQty")
    @TableField("dQty")
    @JsonProperty("dQty")
    private Double dQty;

    @Column(name = "dWasteQty")
    @TableField("dWasteQty")
    @JsonProperty("dWasteQty")
    private Double dWasteQty;

    @Column(name = "dOutPutQty")
    @TableField("dOutPutQty")
    @JsonProperty("dOutPutQty")
    private Double dOutPutQty;

    @Column(name = "uProductionTaskGuid")
    @TableField("uProductionTaskGuid")
    @JsonProperty("uProductionTaskGuid")
    private String  uProductionTaskGuid;

    @Column(name = "uSourceGuid")
    @TableField("uSourceGuid")
    @JsonProperty("uSourceGuid")
    private String  uSourceGuid;

    @Column(name = "uProcessGuid")
    @TableField("uProcessGuid")
    @JsonProperty("uProcessGuid")
    private String  uProcessGuid;

    @Column(name = "sPrintMethod")
    @TableField("sPrintMethod")
    @JsonProperty("sPrintMethod")
    private String  sPrintMethod;

    @Column(name = "iColorNumber")
    @TableField("iColorNumber")
    @JsonProperty("iColorNumber")
    private Integer iColorNumber;

    @Column(name = "iModulus")
    @TableField("iModulus")
    @JsonProperty("iModulus")
    private Integer iModulus;

    @Column(name = "iCutNumber")
    @TableField("iCutNumber")
    @JsonProperty("iCutNumber")
    private Integer iCutNumber;

    @Column(name = "dCutLength")
    @TableField("dCutLength")
    @JsonProperty("dCutLength")
    private Double dCutLength;

    @Column(name = "dCutWidth")
    @TableField("dCutWidth")
    @JsonProperty("dCutWidth")
    private Double dCutWidth;

    @Column(name = "sSalesOrderNo")
    @TableField("sSalesOrderNo")
    @JsonProperty("sSalesOrderNo")
    private String  sSalesOrderNo;

    @Column(name = "sCustomerPO")
    @TableField("sCustomerPO")
    @JsonProperty("sCustomerPO")
    private String  sCustomerPO;

    @Column(name = "sRemark")
    @TableField("sRemark")
    @JsonProperty("sRemark")
    private String  sRemark;

    @Column(name = "tStartTime")
    @TableField("tStartTime")
    @JsonProperty("tStartTime")
    private Date tStartTime;

    @Column(name = "tEndTime")
    @TableField("tEndTime")
    @JsonProperty("tEndTime")
    private Date tEndTime;

    @Column(name = "dProduceHour")
    @TableField("dProduceHour")
    @JsonProperty("dProduceHour")
    private Double dProduceHour;

    @Column(name = "dAdjustHour")
    @TableField("dAdjustHour")
    @JsonProperty("dAdjustHour")
    private Double dAdjustHour;

    @Column(name = "dWaitHour")
    @TableField("dWaitHour")
    @JsonProperty("dWaitHour")
    private Double dWaitHour;

    @Column(name = "dProofreadHour")
    @TableField("dProofreadHour")
    @JsonProperty("dProofreadHour")
    private Double dProofreadHour;

    @Column(name = "dOtherHour")
    @TableField("dOtherHour")
    @JsonProperty("dOtherHour")
    private Double dOtherHour;

    @Column(name = "bPieceWork")
    @TableField("bPieceWork")
    @JsonProperty("bPieceWork")
    private String  bPieceWork;

    @Column(name = "bForcePieceWork")
    @TableField("bForcePieceWork")
    @JsonProperty("bForcePieceWork")
    private String  bForcePieceWork;

    @Column(name = "sTs")
    @TableField("sTs")
    @JsonProperty("sTs")
    private String  sTs;

}
