package com.cz.workorder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.jwt.APIResult;
import com.cz.workorder.dto.WorkOrderListReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/workOrder")
public class WorkOrderApi {

	@Resource
	WorkOrderRepository repo;

	@Resource
	JdbcTemplate jdbcTemplate;

	// @GetMapping
	// public APIResult findPage(String name, Pageable pageable) {
	// 	log.debug("REST request to get Person by : {}", name);
	// 	return APIResult.ok(repo.findBooks());
	// }

	@GetMapping
	public APIResult findPage(Long current, WorkOrderListReq params) {
		log.debug("REST request to get Person by : {}", params);
		System.out.println(String.format("REST request to get Person by : %s", params));
		// return APIResult.ok(repo.findPagingData(pageable));
		return APIResult.ok(repo.findPage(new Page(current, 10), params));
	}

	@GetMapping("todo")
	public APIResult findTodo(Long current, String sBillNo) {
		log.debug("REST request to get Person by : {}", sBillNo);
		System.out.println(String.format("REST request to get Person by : %s", sBillNo));
		// return APIResult.ok(repo.findPagingData(pageable));
		return APIResult.ok(repo.findByTodo(new Page(current, 10), sBillNo));
	}
}
