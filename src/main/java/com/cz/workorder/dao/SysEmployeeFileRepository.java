package com.cz.workorder.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.workorder.dto.SysEmployeeFileRequest;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;


public interface SysEmployeeFileRepository extends BaseMapper {

    @Select("select sID as sEmployeeID,sName as sEmployeeName,sPositionID,sPositionName,iOrder," +
            " sDepartmentID, sDepartmentName, null as bCheck " +
            " from Sys_EmployeeFiles t ${ew.customSqlSegment}")
    Page<Map<String,Object>> _searchEmployee(Page page, @Param(Constants.WRAPPER) Wrapper<Map<String,Object>> wrapper);


    default Page<Map<String,Object>> searchEmployee(Page page, SysEmployeeFileRequest params) {
        QueryWrapper<Map<String,Object>> query = new QueryWrapper<Map<String,Object>>();
        query.ne("t.sJobStatus","离职");
        query.eq("t.bVoid", 0);
        if(StrUtil.isNotBlank(params.getSName())){
            query.like("t.sName", params.getSName());
        }
        return this._searchEmployee(page, query);
    }

}
