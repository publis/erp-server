package com.cz.workorder.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cz.workorder.entity.ManProductionRegistrationDetail;


public interface ManProductionRegistrationDetailRepository extends BaseMapper<ManProductionRegistrationDetail> {

}
