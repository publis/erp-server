package com.cz.workorder.dao;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.workorder.dto.ManProductionRegistrationRequest;
import com.cz.workorder.entity.ManProductionRegistration;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;


public interface ManProductionRegistrationRepository extends BaseMapper<ManProductionRegistration> {

    @Select("select * from View_Man_ProductionRegistration t ${ew.customSqlSegment}")
    Page<Map<String,Object>> _findByPages(Page page, @Param(Constants.WRAPPER) Wrapper<Map<String,Object>> wrapper);


    default Page<Map<String,Object>> findPage(Page page, ManProductionRegistrationRequest params) {
        QueryWrapper<Map<String,Object>> query = new QueryWrapper<Map<String,Object>>();
        query.eq("t.sReportSource","APP02");
        final DateTime endTime = DateUtil.parse(DateUtil.format(params.getBillTimeEnd(), "yyyy-MM-dd") + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
        query.between("t.tBillTime", params.getBillTimeStart(), endTime);
        if(StrUtil.isNotBlank(params.getApp()) && StrUtil.equalsAny(params.getApp(),"0","1")){
            query.eq("t.bAPP",params.getApp());
        }
        if(StrUtil.isNotBlank(params.getBillNo())){
            query.like("t.sBillNo", "%"+params.getBillNo()+"%");
        }
        if(StrUtil.isNotBlank(params.getWorkOrderNo())){
            query.like("t.sWorkOrderNo", "%"+params.getWorkOrderNo()+"%");
        }
        if(StrUtil.isNotBlank(params.getBarCode())){
            query.eq("t.uBarCode", params.getBarCode());
        }
        if(StrUtil.isNotBlank(params.getWorkCenterName())){
            query.eq("t.sWorkCenterName", params.getWorkCenterName());
        }
        if(StrUtil.isNotBlank(params.getMachineName())){
            query.eq("t.sMachineName", params.getMachineName());
        }
        if(StrUtil.isNotBlank(params.getTeamName())){
            query.eq("t.sTeamName", params.getTeamName());
        }
        if(StrUtil.isNotBlank(params.getProductID())){
            query.eq("t.sProductID", params.getProductID());
        }
        if(StrUtil.isNotBlank(params.getProductName())){
            query.eq("t.sProductName", params.getProductName());
        }
        query.orderByDesc("t.tBillTime");
        return this._findByPages(page, query);
    }



    /**
     * uGuid1，
     * 工单号，客户编码，客户名称，客户PO号，产品编码，
     * 产品名称，产品规格，部件名称，工序编码，工序名称，
     * 待报产数，
     * 投入数（取待报产数），
     * 产出数（取待报产数），
     * 源单类型，生产数量，工序数量，生产单位编码，
     * 生产单位，工序识别码，印刷方式，色数，拼版模数，
     * 开料开数，开料长mm，开料宽mm，销售单号
     */
    @Select("select t1.uGuid1, " +
            " t1.sWorkOrderNo,t1.sCustomerID,t1.sCustomerName,t1.sCustomerPO,t1.sProductID, " +
            " t1.sProductName,t1.sProductSize,t1.sPartName,t1.sProcessID,t1.sProcessName, " +
            " isnull(t1.dPlanQty, 0) - isnull(t1.dPlanOutQty, 0) - isnull(t1.dReportQty, 0) as dNoReportQty, " +
            " isnull(t1.dPlanQty, 0) - isnull(t1.dPlanOutQty, 0) - isnull(t1.dReportQty, 0) as dQty, " +
            " isnull(t1.dPlanQty, 0) - isnull(t1.dPlanOutQty, 0) - isnull(t1.dReportQty, 0) as dQutPutQty, " +
            " 'frm_ProductionTask' as sSourceType,t1.dProductQty,t1.dProcessQty,t1.sProductionUnitID, " +
            " t1.sProductionUnitName,t1.uProcessGuid,t1.sPrintMethod,t1.iColorNumber,t1.iModulus, " +
            " t1.iCutNumber,t1.dCutLength,t1.dCutWidth,t1.sSalesOrderNo " +
            " from Man_ProductionTask (nolock) t1 " +
            " left join Sal_ProductFiles (nolock) t4 on t4.sID = t1.sProductID " +
            " left join Man_ProcessFiles (nolock) t5 on t1.sProcessID = t5.sID " +
            " ${ew.customSqlSegment}")
    Page<Map<String, Object>> _findByTodoTask(Page page, @Param(Constants.WRAPPER) Wrapper<Map<String,Object>> wrapper);

    default Page<Map<String, Object>> findByTodoTask(Page page, String sBillNo) {
        QueryWrapper<Map<String,Object>> query = new QueryWrapper<Map<String,Object>>();
        query.eq("isnull(t1.bForceReport, 0)",0);
        query.eq("t5.bReport",1);
        query.gt("isnull(t1.dPlanQty, 0) - isnull(t1.dPlanOutQty, 0) - isnull(t1.dReportQty, 0)", 0);
        if(StrUtil.isNotBlank(sBillNo)){
            query.like("t1.sWorkOrderNo", sBillNo);
        }
        return this._findByTodoTask(page, query);
    }

    /**
     *  uGuid1
     *  工单号，源单类型，客户编码，客户名称，销售单号，
     *  客户PO号，产品编码，产品名称，产品规格，生产单位编码，
     *  生产单位，生产数量，部件名称，工序编码，工序名称，
     *  工序数量，
     *  待报产数，
     *  任务识别码，工序识别码，印刷方式，色数，拼版模数，
     *  开料开数，开料长mm，开料宽mm
     */
    @Select(" select t1.uGuid1, " +
            " t1.sWorkOrderNo,t1.sSourceType,t1.sCustomerID,t1.sCustomerName,t1.sSalesOrderNo, " +
            " t1.sCustomerPO,t1.sProductID,t1.sProductName,t1.sProductSize,t1.sProductionUnitID, " +
            " t1.sProductionUnitName,t1.dProductQty,t1.sPartName,t1.sProcessID,t1.sProcessName, " +
            " t1.dPlanQty as dProcessQty, " +
            " t1.dPlanQty-ISNULL(t1.dPlanOutQty,0)-ISNULL(t1.dReportQty,0) as dNoReportQty, " +
            " t1.uGuid1 as uBarCode,t1.uProcessGuid,t1.sPrintMethod,t1.iColorNumber,t1.iModulus, " +
            " t1.iCutNumber,t1.dCutLength,t1.dCutWidth," +
            " '' as sBillNo" +
            " from Man_ProductionTask t1 (nolock) " +
            " ${ew.customSqlSegment}")
    Map<String, Object> _findByProductionTask(@Param(Constants.WRAPPER) Wrapper<Map<String,Object>> wrapper);

    default Map<String, Object> findByProductionTask(String uBarCode) {
        QueryWrapper<Map<String,Object>> query = new QueryWrapper<Map<String,Object>>();
        query.eq("cast(t1.uGuid1 as varchar(40))",uBarCode);
        return this._findByProductionTask(query);
    }
}
