package com.cz.workorder.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cz.workorder.entity.ManProductionRegistrationDetailEmployee;


public interface ManProductionRegistrationDetailEmployeeRepository extends BaseMapper<ManProductionRegistrationDetailEmployee> {

}
