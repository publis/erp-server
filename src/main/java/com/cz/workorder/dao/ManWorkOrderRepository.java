package com.cz.workorder.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cz.workorder.entity.ManWorkOrder;


public interface ManWorkOrderRepository extends BaseMapper<ManWorkOrder> {

}
