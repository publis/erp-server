package com.cz.workorder.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderListReq {
    String tBillTimeStart;
    String tBillTimeEnd;
    String bAPP;
    String sBillNo;
    String sWorkOrderNo;
    String uBarCode;
    String sWorkCenterName;
    String sMachineName;
    String sTeamName;
    String sProductID;
    String sProductName;
}