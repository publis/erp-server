package com.cz.workorder.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManProductionRegistrationRequest {

    @NotNull(message = "请输入开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date billTimeStart;

    @NotNull(message = "请输入结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date billTimeEnd;

    @NotNull(message = "请选择是否审核")
    private String app;

    private String billNo;

    private String workOrderNo;

    private String barCode;

    private String workCenterName;

    private String machineName;

    private String teamName;

    private String productID;

    private String productName;
}
