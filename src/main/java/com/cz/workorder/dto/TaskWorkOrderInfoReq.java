package com.cz.workorder.dto;

import com.cz.workorder.entity.ManProductionRegistration;
import com.cz.workorder.entity.ManProductionRegistrationDetail;
import com.cz.workorder.entity.ManProductionRegistrationDetailEmployee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskWorkOrderInfoReq {
    private ManProductionRegistration registration;
    private ManProductionRegistrationDetail detail;
    private List<ManProductionRegistrationDetailEmployee> employeeList;
}