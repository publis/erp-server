package com.cz.workorder.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductionTaskRequest {

    @JsonProperty("sBillNo")
    private String sBillNo;

    @NotNull(message = "工单号不能为空!")
    @JsonProperty("sWorkOrderNo")
    private String sWorkOrderNo;

    @NotNull(message = "工作中心不能为空!")
    @JsonProperty("sWorkCenterID")
    private String sWorkCenterID;

    @JsonProperty("sWorkCenterName")
    private String sWorkCenterName;

    @NotNull(message = "机台名称不能为空!")
    @JsonProperty("sMachineID")
    private String sMachineID;

    @JsonProperty("sMachineName")
    private String sMachineName;

    @JsonProperty("uBarCode")
    private String uBarCode;

    @JsonProperty("sCustomerID")
    private String sCustomerID;

    @JsonProperty("sCustomerName")
    private String sCustomerName;

    @JsonProperty("sCustomerPO")
    private String sCustomerPO;

    @NotNull(message = "产品名称不能为空!")
    @JsonProperty("sProductID")
    private String sProductID;

    @JsonProperty("sProductName")
    private String sProductName;

    @JsonProperty("sProductSize")
    private String sProductSize;

    @JsonProperty("sPartName")
    private String sPartName;

    @NotNull(message = "工序名称不能为空!")
    @JsonProperty("sProcessID")
    private String sProcessID;

    @JsonProperty("sProcessName")
    private String sProcessName;

    @NotNull(message = "待报产数不能为空!")
    @JsonProperty("dNoReportQty")
    private Double dNoReportQty;

    @NotNull(message = "计数方式不能为空!")
    @JsonProperty("sCountType")
    private String sCountType;

    @JsonProperty("sCountTypeName")
    private String sCountTypeName;

    @NotNull(message = "投入数不能为空!")
    @JsonProperty("dQty")
    private Double dQty;

    @NotNull(message = "产出数不能为空!")
    @JsonProperty("dOutPutQty")
    private Double dOutPutQty;

    @NotNull(message = "废品数不能为空!")
    @JsonProperty("dWasteQty")
    private Double dWasteQty;

    @JsonProperty("sTeamID")
    private String sTeamID;

    @JsonProperty("sTeamName")
    private String sTeamName;

    @NotNull(message = "源单类型不能为空!")
    @JsonProperty("sSourceType")
    private String sSourceType;

    @NotNull(message = "生产数量不能为空!")
    @JsonProperty("dProductQty")
    private Double dProductQty;

    @NotNull(message = "工序数量不能为空!")
    @JsonProperty("dProcessQty")
    private Double dProcessQty;

    @JsonProperty("sProductionUnitID")
    private String sProductionUnitID;

    @JsonProperty("sProductionUnitName")
    private String sProductionUnitName;

    @NotNull(message = "工序识别码不能为空!")
    @JsonProperty("uProcessGuid")
    private String uProcessGuid;

    @JsonProperty("sPrintMethod")
    private String sPrintMethod;

    @JsonProperty("iColorNumber")
    private Integer iColorNumber;

    @JsonProperty("iModulus")
    private Integer iModulus;

    @JsonProperty("iCutNumber")
    private Integer iCutNumber;

    @JsonProperty("dCutLength")
    private Double dCutLength;

    @JsonProperty("dCutWidth")
    private Double dCutWidth;

    @JsonProperty("sSalesOrderNo")
    private String sSalesOrderNo;

    @JsonProperty("userTask")
    private List<ProductionTaskUser> userTask;

}
