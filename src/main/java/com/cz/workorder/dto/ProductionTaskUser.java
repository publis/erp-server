package com.cz.workorder.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductionTaskUser {

    @NotNull(message = "员工工号不能为空!")
    @JsonProperty("sEmployeeID")
    private String sEmployeeID;

    @NotNull(message = "员工名称不能为空!")
    @JsonProperty("sEmployeeName")
    private String sEmployeeName;

    @NotNull(message = "部门编号不能为空!")
    @JsonProperty("sDepartmentID")
    private String sDepartmentID;

    @NotNull(message = "部门名称不能为空!")
    @JsonProperty("sDepartmentName")
    private String sDepartmentName;

    @NotNull(message = "岗位编号不能为空!")
    @JsonProperty("sPositionID")
    private String sPositionID;

    @NotNull(message = "岗位名称不能为空!")
    @JsonProperty("sPositionName")
    private String sPositionName;

    @NotNull(message = "投入数不能为空!")
    @JsonProperty("dQty")
    private Double dQty;

    @NotNull(message = "废品数不能为空!")
    @JsonProperty("dWasteQty")
    private Double dWasteQty;

    @NotNull(message = "产出数不能为空!")
    @JsonProperty("dOutPutQty")
    private Double dOutPutQty;

    @JsonProperty("iOrder")
    private Integer iOrder;
}
