package com.cz.comdict;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.util.StringUtils;

public interface SystemOptionRepository extends BaseMapper {

    @Select(" select D.sID as code,D.sName as name " +
            " from Sys_SystemOption O,Sys_SystemOption_Detail D " +
            " ${ew.customSqlSegment}")
    Page<DictResponse> _searchDict(Page page, @Param(Constants.WRAPPER) Wrapper<DictResponse> wrapper);

    default Page<DictResponse> searchDict(Page page, String dictCode, String name) {
        QueryWrapper<DictResponse> query = new QueryWrapper<DictResponse>();
        query.apply(" O.uGuid1 = D.uGuid1 ");
        query.eq(" O.sID", dictCode);
        if (StringUtils.hasText(name)) {
            query.like("D.sName", name);
        }
        query.orderByAsc(" D.iOrder");
        return this._searchDict(page, query);
    }
}
