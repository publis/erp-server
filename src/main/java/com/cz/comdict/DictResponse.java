package com.cz.comdict;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DictResponse implements Serializable, Cloneable{
    private String code;
    private String name;
}
