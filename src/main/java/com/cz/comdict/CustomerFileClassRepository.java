package com.cz.comdict;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.util.StringUtils;

public interface CustomerFileClassRepository extends BaseMapper {
    @Select("select pr.uGuid1 as code, pr.sName as name from Sal_CustomerFileClass pr ${ew.customSqlSegment}")
    Page<DictResponse> _searchDict(Page page, @Param(Constants.WRAPPER) Wrapper<DictResponse> wrapper);

    default Page<DictResponse> searchDict(Page page, String name) {
        QueryWrapper<DictResponse> query = new QueryWrapper<DictResponse>();
        query.apply(" pr.sName is not null");
        query.apply(" pr.bVoid=0 ");
        if (StringUtils.hasText(name)) {
            query.apply("pr.sName like {0}", "%" + name +"%");
        }
        query.orderByAsc(" pr.iOrder");
        return this._searchDict(page, query);
    }
}
