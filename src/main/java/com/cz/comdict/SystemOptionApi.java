package com.cz.comdict;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cz.core.jwt.APIResult;
import com.cz.uitl.PageConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/comdict/systemoption")
public class SystemOptionApi {

    @Resource
    private SystemOptionRepository repo;

    @GetMapping("search")
    public APIResult search(Long current, String dictCode, String sName) {
        return APIResult.ok(repo.searchDict(new Page(current, PageConst.pageSize), dictCode, sName));
    }
}
