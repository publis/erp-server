package com.cz.comdict;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.util.StringUtils;

import java.util.Map;

public interface SaleProductRepository extends BaseMapper {
    @Select("select * from Sal_ProductFiles pr ${ew.customSqlSegment}")
    Page<Map<String, Object>> _findByPages(Page page, @Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

    default Page<Map<String, Object>> findPageByName(Page page, String name) {
        LambdaQueryWrapper<Map<String, Object>> query = new QueryWrapper<Map<String, Object>>().lambda();
        if (StringUtils.hasText(name)) {
            query.apply("pr.sName like {0}", "%" + name +"%");
        }
        return this._findByPages(page, query);
    }

    @Select("select pr.sID as code, pr.sName as name from Sal_ProductFiles pr ${ew.customSqlSegment}")
    Page<DictResponse> _searchDict(Page page, @Param(Constants.WRAPPER) Wrapper<DictResponse> wrapper);

    default Page<DictResponse> searchDict(Page page, String name) {
        QueryWrapper<DictResponse> query = new QueryWrapper<DictResponse>();
        query.apply(" pr.sName is not null");
        query.apply(" pr.bApp=1 ");
        query.apply(" pr.bVoid=0 ");
        if (StringUtils.hasText(name)) {
            query.apply("pr.sName like {0}", "%" + name +"%");
        }
        query.orderByAsc(" pr.iOrder");
        return this._searchDict(page, query);
    }

    @Select("select pr.sID as code, pr.sID as name from Sal_ProductFiles pr ${ew.customSqlSegment}")
    Page<DictResponse> _idSearchDict(Page page, @Param(Constants.WRAPPER) Wrapper<DictResponse> wrapper);

    default Page<DictResponse> idSearchDict(Page page, String id) {
        QueryWrapper<DictResponse> query = new QueryWrapper<DictResponse>();
        query.apply(" pr.sID is not null");
        query.apply(" pr.bApp=1 ");
        query.apply(" pr.bVoid=0 ");
        if (StringUtils.hasText(id)) {
            query.apply("pr.sID like {0}", "%" + id +"%");
        }
        query.orderByAsc(" pr.iOrder");
        return this._idSearchDict(page, query);
    }
}
